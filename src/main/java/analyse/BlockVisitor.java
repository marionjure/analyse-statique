package analyse;

import ast.Boolean;
import ast.*;

import java.util.ArrayList;
import java.util.List;

import static support.ListTools.singleton;

/**
 * Récupérer que les blocks (affectation, condition, skip)
 */

public class BlockVisitor implements Visitor<List<Node>> {

    @Override
    public List<Node> visit(Program program) {
        List<Node> nodes = new ArrayList<>();
        if (program.getName().isPresent())
            nodes.addAll(program.getName().get().accept(this));
        for (Declaration declaration : program.getDeclarations())
            nodes.addAll(declaration.accept(this));
        nodes.addAll(program.getStatements().accept(this));
        return nodes;
    }

    @Override
    public List<Node> visit(AExpIdent aExpIdent) {
        return aExpIdent.getIdentifier().accept(this);
    }

    @Override
    public List<Node> visit(AExpConst aExpConst) {
        return aExpConst.getConstant().accept(this);
    }

    @Override
    public List<Node> visit(AExpBinary aExpBinary) {
        List<Node> nodes = new ArrayList<>();
        nodes.addAll(aExpBinary.getAexpressionLeft().accept(this));
        nodes.addAll(aExpBinary.getAexpressionRight().accept(this));
        return nodes;

    }

    @Override
    public List<Node> visit(AExpOpposite aExpOpposite) {
        return aExpOpposite.getAexpression().accept(this);
    }

    @Override
    public List<Node> visit(AExpParenthesis aExpParenthesis) {
        return aExpParenthesis.getAexpression().accept(this);
    }

    @Override
    public List<Node> visit(BExpTrue bExpTrue) {
        return singleton(bExpTrue);
    }

    @Override
    public List<Node> visit(BExpFalse bExpFalse) {
        return singleton(bExpFalse);
    }

    @Override
    public List<Node> visit(BExpBinary bExpBinary) {
        return singleton(bExpBinary);

    }

    @Override
    public List<Node> visit(BExpNot bExpNot) {
        return singleton(bExpNot);
    }

    @Override
    public List<Node> visit(BExpParenthesis bExpParenthesis) {
        return singleton(bExpParenthesis);
    }

    @Override
    public List<Node> visit(BlockOnly blockOnly) {
        return blockOnly.getStatement().accept(this);
    }

    @Override
    public List<Node> visit(BlockParenthesis blockParenthesis) {
        return blockParenthesis.getStatements().accept(this);
    }

    @Override
    public List<Node> visit(Declaration declaration) {
        List<Node> nodes = new ArrayList<>();
        //nodes.add(declaration);
        nodes.add(declaration.getBegin());
        nodes.add(declaration.getEnd());
        nodes.addAll(declaration.getName().accept(this));
        nodes.addAll(declaration.getlDeclIdent().accept(this));
        if (declaration.getTypeRes().isPresent())
            nodes.addAll(declaration.getTypeRes().get().accept(this));
        if (declaration.getNameRes().isPresent())
            nodes.addAll(declaration.getNameRes().get().accept(this));
        nodes.addAll(declaration.getStatements().accept(this));
        return nodes;

    }

    @Override
    public List<Node> visit(LDeclIdent lDeclIdent) {
        List<Node> nodes = new ArrayList<>();

        for (Type type : lDeclIdent.getTypes())
            nodes.addAll(type.accept(this));
        for (Identifier parameter : lDeclIdent.getParameters())
            nodes.addAll(parameter.accept(this));
        return nodes;
    }

    @Override
    public List<Node> visit(LDeclVariables lDeclVariables) {
        List<Node> nodes = new ArrayList<>();
        lDeclVariables.getDeclVariables().accept(this);
        for (LDeclVariables lDeclVariable : lDeclVariables.getlDeclVariables()) {
            lDeclVariable.accept(this);
        }
        return nodes;
    }

    @Override
    public List<Node> visit(DeclVariables declVariables) {
        List<Node> nodes = new ArrayList<>();
        declVariables.getType().accept(this);
        declVariables.getlIdentifier().accept(this);
        return nodes;
    }

    @Override
    public List<Node> visit(LIdentifier lIdentifier) {
        List<Node> nodes = new ArrayList<>();
        for (Identifier identifier : lIdentifier.getIdentifiers())
            nodes.addAll(identifier.accept(this));
        return nodes;
    }

    @Override
    public List<Node> visit(Statements statements) {
        List<Node> nodes = new ArrayList<>(statements.getStatement().accept(this));
        for (Statements statement : statements.getStatements())
            nodes.addAll(statement.accept(this));

        return nodes;
    }

    @Override
    public List<Node> visit(Statement statement) {
        return statement.accept(this);
    }

    @Override
    public List<Node> visit(LAexpression lAexpression) {
        List<Node> nodes = new ArrayList<>();
        return nodes;
    }

    @Override
    public List<Node> visit(Identifier identifier) {
        return new ArrayList<>();
    }

    @Override
    public List<Node> visit(Constant constant) {
        return new ArrayList<>();
    }

    @Override
    public List<Node> visit(End end) {
        return new ArrayList<>();
    }

    @Override
    public List<Node> visit(Begin begin) {
        return new ArrayList<>();
    }

    @Override
    public List<Node> visit(Skip skip) {
        return singleton(skip);
    }

    @Override
    public List<Node> visit(Affectation affectation) {
        return singleton(affectation);
    }

    @Override
    public List<Node> visit(If sif) {
        List<Node> nodes = new ArrayList<>();
        nodes.addAll(sif.getBexpression().accept(this));
        nodes.addAll(sif.getIfBlock().accept(this));
        if (sif.getElseBlock().isPresent())
            nodes.addAll(sif.getElseBlock().get().accept(this));
        return nodes;
    }

    @Override
    public List<Node> visit(While swhile) {
        List<Node> nodes = new ArrayList<>();
        nodes.addAll(swhile.getBexpression().accept(this));
        nodes.addAll(swhile.getBlock().accept(this));
        return nodes;
    }

    @Override
    public List<Node> visit(Call call) {
        return singleton(call);
    }

    @Override
    public List<Node> visit(Int tint) {
        return new ArrayList<>();
    }

    @Override
    public List<Node> visit(Boolean tboolean) {
        return singleton(tboolean);
    }
}
