package analyse;

import ast.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Donne le noeud final d'un programme
 */

public class FinalVisitor extends DefaultVisitor<List<Integer>> {
    public FinalVisitor(List<Integer> defaultValue) {
        super(defaultValue);
    }

    @Override
    public List<Integer> visit(Skip skip) {
        return List.of(skip.getLabel());
    }

    @Override
    public List<Integer> visit(Affectation affectation) {
        return List.of(affectation.getLabel());
    }

    @Override
    public List<Integer> visit(If sif) {
        // ajouter le label du if
        List<Integer> list = new ArrayList<>(sif.getIfBlock().accept(this));
        // s'il y a un block else, ajouter le label du else
        if (sif.getElseBlock().isPresent())
            list.addAll(sif.getElseBlock().get().accept(this));
            // sinon, ajouter le label de la condition au début
        else
            list.add(0, sif.getLabel());
        return list;
    }

    @Override
    public List<Integer> visit(While swhile) {
        return List.of(swhile.getLabel());
    }

    @Override
    public List<Integer> visit(Call call) {
        return List.of(call.getLabelOut());
    }

    @Override
    public List<Integer> visit(Program program) {
        List<Integer> res = new ArrayList<>();
        for (Declaration d : program.getDeclarations())
            res = d.accept(this);
        res = program.getStatements().accept(this);
        return res;
    }

    @Override
    public List<Integer> visit(BlockOnly blockOnly) {
        return blockOnly.getStatement().accept(this);
    }

    @Override
    public List<Integer> visit(BlockParenthesis blockParenthesis) {
        return blockParenthesis.getStatements().accept(this);
    }

    @Override
    public List<Integer> visit(Declaration declaration) {
        return List.of(declaration.getLabelOut());
    }

    @Override
    public List<Integer> visit(Statements statements) {
        List<Integer> res = statements.getStatement().accept(this);
        for (Statements s : statements.getStatements()) {
            res = s.accept(this);
        }
        return res;
    }
}
