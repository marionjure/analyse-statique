package analyse;

import ast.*;
import support.Pair;

import java.lang.Boolean;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Récupérer toutes les déclarations d'une variable pour l'analyse RD
 */

public class DeclarationVisitor extends DefaultVisitor<Map<Identifier, Set<Pair<Identifier, Integer>>>> {
    private final Map<Identifier, Boolean> declarationMap;

    public DeclarationVisitor() {
        super(new HashMap<>());
        this.declarationMap = new HashMap<>();
    }

    public Map<Identifier, Set<Pair<Identifier, Integer>>> visit(Declaration declaration, Program program) {
        for (Declaration declaration1 : program.getDeclarations()) {
            if (declaration1.getNameRes().isPresent()) {
                this.declarationMap.put(declaration1.getName(), true);
            } else {
                this.declarationMap.put(declaration1.getName(), false);
            }
        }
        return declaration.getStatements().accept(this);
    }

    @Override
    public Map<Identifier, Set<Pair<Identifier, Integer>>> visit(Declaration declaration) {
        return declaration.getStatements().accept(this);
    }

    @Override
    public Map<Identifier, Set<Pair<Identifier, Integer>>> visit(Program program) {
        for (Declaration declaration1 : program.getDeclarations()) {
            if (declaration1.getNameRes().isPresent()) {
                this.declarationMap.put(declaration1.getName(), true);
            } else {
                this.declarationMap.put(declaration1.getName(), false);
            }
        }
        return program.getStatements().accept(this);
    }

    @Override
    public Map<Identifier, Set<Pair<Identifier, Integer>>> visit(Statements statements) {
        Map<Identifier, Set<Pair<Identifier, Integer>>> l = new HashMap<>();
        l.putAll(statements.getStatement().accept(this));
        for (Statements statements1 : statements.getStatements()) {
            for (var entry : (statements1.accept(this)).entrySet()) {
                if (!l.containsKey(entry.getKey())) {
                    l.put(entry.getKey(), new HashSet<>());
                }
                for (var valeur : entry.getValue()) {
                    l.get(entry.getKey()).add(valeur);
                }
            }
        }
        return l;
    }

    @Override
    public Map<Identifier, Set<Pair<Identifier, Integer>>> visit(Call call) {
        Map<Identifier, Set<Pair<Identifier, Integer>>> l = new HashMap<>();
        if (this.declarationMap.get(call.getIdentifier())) {
            Identifier identifier = ((AExpIdent) call.getlAexpression().getAexpressionList().get(call.getlAexpression().getAexpressionList().size() - 1)).getIdentifier();
            l.put(identifier, new HashSet<>());
            l.get(identifier).add(new Pair<>(identifier, null));
            l.get(identifier).add(new Pair<>(identifier, call.getLabelOut()));
        }
        return l;
    }

    @Override
    public Map<Identifier, Set<Pair<Identifier, Integer>>> visit(While swhile) {
        return swhile.getBlock().accept(this);
    }

    @Override
    public Map<Identifier, Set<Pair<Identifier, Integer>>> visit(If sif) {
        Map<Identifier, Set<Pair<Identifier, Integer>>> l = new HashMap<>();
        l.putAll(sif.getIfBlock().accept(this));
        if (sif.getElseBlock().isPresent()) {
            for (var entry : (sif.getElseBlock().get().accept(this)).entrySet()) {
                if (!l.containsKey(entry.getKey())) {
                    l.put(entry.getKey(), new HashSet<>());
                }
                for (var valeur : entry.getValue()) {
                    l.get(entry.getKey()).add(valeur);
                }
            }
        }
        return l;
    }

    @Override
    public Map<Identifier, Set<Pair<Identifier, Integer>>> visit(Affectation affectation) {
        Map<Identifier, Set<Pair<Identifier, Integer>>> l = new HashMap<>();
        l.put(affectation.getIdentifier(), new HashSet<>());
        l.get(affectation.getIdentifier()).add(new Pair<>(affectation.getIdentifier(), null));
        l.get(affectation.getIdentifier()).add(new Pair<Identifier, Integer>(affectation.getIdentifier(), affectation.getLabel()));
        return l;
    }


}
