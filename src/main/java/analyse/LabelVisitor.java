package analyse;

import ast.*;
import support.Void;

import java.util.ArrayList;

/**
 * Donne le label des blocks
 */

public class LabelVisitor extends DefaultVisitor<Void> {

    private int cptLabel;
    private final ArrayList<Integer> list;

    public LabelVisitor(Void defaultValue) {
        super(defaultValue);
        this.cptLabel = 1;
        list = new ArrayList<>();
    }

    @Override
    public Void visit(Skip skip) {
        this.list.add(cptLabel);
        skip.setLabel(cptLabel++);
        return defaultValue;
    }

    @Override
    public Void visit(Affectation affectation) {
        this.list.add(cptLabel);
        affectation.setLabel(cptLabel++);
        return defaultValue;
    }

    @Override
    public Void visit(If sif) {
        this.list.add(cptLabel);
        sif.getBexpression().setLabel(cptLabel);
        sif.getBexpression().accept(this);
        sif.setLabel(cptLabel++);
        Void curr = sif.getIfBlock().accept(this);
        if (sif.getElseBlock().isPresent())
            curr = sif.getElseBlock().get().accept(this);
        return curr;
    }

    @Override
    public Void visit(While swhile) {
        this.list.add(cptLabel);
        swhile.getBexpression().setLabel(cptLabel);
        swhile.getBexpression().accept(this);
        swhile.setLabel(cptLabel++);
        swhile.getBexpression().accept(this);
        return swhile.getBlock().accept(this);
    }

    @Override
    public Void visit(Call call) {
        this.list.add(cptLabel);
        call.setLabel(cptLabel++);
        call.getIdentifier().accept(this);
        Void v = call.getlAexpression().accept(this);
        this.list.add(cptLabel);
        call.setLabelOut(cptLabel++);
        return v;
    }

    public ArrayList<Integer> getList() {
        return list;
    }

    @Override
    public Void visit(Declaration declaration) {
        this.list.add(cptLabel);
        declaration.setLabelIn(cptLabel++);
        declaration.getName().accept(this);
        declaration.getlDeclIdent().accept(this);
        if (declaration.getTypeRes().isPresent())
            declaration.getTypeRes().get().accept(this);
        if (declaration.getNameRes().isPresent())
            declaration.getNameRes().get().accept(this);
        Void v = declaration.getStatements().accept(this);
        this.list.add(cptLabel);
        declaration.setLabelOut(cptLabel++);
        return v;
    }
}
