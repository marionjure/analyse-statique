package analyse;

import ast.*;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

/**
 * Mapper une variable à une autre
 */

public class ChangerValeur2Visitor extends DefaultVisitor<Node> {

    private Map<Identifier, Aexpression> map;

    public ChangerValeur2Visitor(Map<Identifier, Aexpression> map) {
        super(null);
        this.map = map;
    }

    @Override
    public Node visit(Declaration declaration) {
        Statements statements = (Statements) declaration.getStatements().accept(this);
        return new Declaration(declaration.getPosition(), declaration.getName(), declaration.getlDeclIdent(), declaration.getTypeRes(), declaration.getNameRes(), statements);
    }

    @Override
    public Node visit(BlockOnly blockOnly) {
        Statement statement = (Statement) blockOnly.getStatement().accept(this);
        return new BlockOnly(blockOnly.getPosition(), statement);
    }

    @Override
    public Node visit(BlockParenthesis blockParenthesis) {
        Statements statements = (Statements) blockParenthesis.getStatements().accept(this);
        return new BlockParenthesis(blockParenthesis.getPosition(), statements);
    }

    @Override
    public Node visit(LAexpression lAexpression) {
        ArrayList<Aexpression> l = new ArrayList<>();
        for (Aexpression aexpression : lAexpression.getAexpressionList()) {
            l.add((Aexpression) aexpression.accept(this));
        }
        return new LAexpression(lAexpression.getPosition(), l);
    }


    @Override
    public Node visit(Statements statements) {
        Statement statement1 = (Statement) statements.getStatement().accept(this);
        ArrayList<Statements> statements1 = new ArrayList<>();
        for (Statements statement : statements.getStatements()) {
            statements1.add((Statements) statement.accept(this));
        }
        return new Statements(statements.getPosition(), statement1, statements1);
    }

    @Override
    public Node visit(Skip skip) {
        return new Skip(skip.getPosition());
    }

    @Override
    public Node visit(Affectation affectation) {
        Aexpression aexpression = (Aexpression) affectation.getAexpression().accept(this);
        Identifier identifier = affectation.getIdentifier();
        return new Affectation(affectation.getPosition(), identifier, aexpression);
    }

    @Override
    public Node visit(If sif) {
        Bexpression bexpression = (Bexpression) sif.getBexpression().accept(this);
        Block block = (Block) sif.getIfBlock().accept(this);
        Optional<Block> else_block = Optional.empty();
        if (sif.getElseBlock().isPresent()) {
            else_block = Optional.ofNullable((Block) sif.getElseBlock().get().accept(this));
        }
        return new If(sif.getPosition(), bexpression, block, else_block);
    }

    @Override
    public Node visit(While swhile) {
        Bexpression bexpression = (Bexpression) swhile.getBexpression().accept(this);
        Block block = (Block) swhile.getBlock().accept(this);
        return new While(swhile.getPosition(), bexpression, block);
    }

    @Override
    public Node visit(Call call) {
        Identifier identifier = call.getIdentifier();
        LAexpression aexpression = (LAexpression) call.getlAexpression().accept(this);
        return new Call(call.getPosition(), identifier, aexpression);
    }

    @Override
    public Node visit(Identifier identifier) {
        if (map.containsKey(identifier)) {
            return map.get(identifier);
        }
        return new Identifier(identifier.getPosition(), identifier.getName());
    }

    @Override
    public Node visit(AExpConst aExpConst) {
        return new AExpConst(aExpConst.getPosition(), aExpConst.getConstant());
    }

    @Override
    public Node visit(AExpBinary aExpBinary) {
        Aexpression right = (Aexpression) aExpBinary.getAexpressionRight().accept(this);
        Aexpression left = (Aexpression) aExpBinary.getAexpressionLeft().accept(this);
        return new AExpBinary(aExpBinary.getPosition(), left, aExpBinary.getBinaryOp(), right);
    }

    @Override
    public Node visit(AExpOpposite aExpOpposite) {
        Aexpression aexpression = (Aexpression) aExpOpposite.getAexpression().accept(this);
        return new AExpOpposite(aExpOpposite.getPosition(), aexpression);
    }

    @Override
    public Node visit(AExpParenthesis aExpParenthesis) {
        Aexpression aexpression = (Aexpression) aExpParenthesis.getAexpression().accept(this);
        return new AExpParenthesis(aExpParenthesis.getPosition(), aexpression);
    }


    @Override
    public Node visit(AExpIdent aExpIdent) {
        if (aExpIdent.getIdentifier().accept(this).getClass().getName().compareTo("ast.Identifier") == 0) {
            Identifier identifier = (Identifier) aExpIdent.getIdentifier().accept(this);
            return new AExpIdent(aExpIdent.getPosition(), identifier);
        } else {
            Aexpression identifier = (Aexpression) aExpIdent.getIdentifier().accept(this);
            return new AExpParenthesis(aExpIdent.getPosition(), identifier);
        }
    }

    @Override
    public Node visit(BExpBinary bExpBinary) {
        Aexpression aexpression = (Aexpression) bExpBinary.getLeft().accept(this);
        Aexpression aexpression1 = (Aexpression) bExpBinary.getRight().accept(this);
        return new BExpBinary(bExpBinary.getPosition(), aexpression, bExpBinary.getOperator(), aexpression1);
    }

    @Override
    public Node visit(BExpNot bExpNot) {
        Bexpression bexpression = (Bexpression) bExpNot.getExpression().accept(this);
        return new BExpNot(bExpNot.getPosition(), bExpNot.getOperation(), bexpression);
    }

    @Override
    public Node visit(BExpParenthesis bExpParenthesis) {
        Bexpression bexpression = (Bexpression) bExpParenthesis.getBexpression().accept(this);
        return new BExpParenthesis(bExpParenthesis.getPosition(), bexpression);
    }

    public void setMap(Map<Identifier, Aexpression> map) {
        this.map = map;
    }
}

