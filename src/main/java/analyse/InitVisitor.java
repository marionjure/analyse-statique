package analyse;

import ast.*;

/**
 * Donne le noeud initial d'un programme
 */

public class InitVisitor extends DefaultVisitor<Integer> {

    public InitVisitor(Integer defaultValue) {
        super(defaultValue);
    }

    @Override
    public Integer visit(Skip skip) {
        return skip.getLabel();
    }

    @Override
    public Integer visit(Affectation affectation) {
        return affectation.getLabel();
    }

    @Override
    public Integer visit(If sif) {
        return sif.getLabel();
    }

    @Override
    public Integer visit(While swhile) {
        return swhile.getLabel();
    }

    @Override
    public Integer visit(Call call) {
        return call.getLabel();
    }


    @Override
    public Integer visit(Program program) {
        /**for (Declaration declaration : program.getDeclarations())
         return declaration.accept(this);*/
        return program.getStatements().accept(this);
    }


    @Override
    public Integer visit(BlockOnly blockOnly) {
        return blockOnly.getStatement().accept(this);
    }

    @Override
    public Integer visit(BlockParenthesis blockParenthesis) {
        return blockParenthesis.getStatements().accept(this);
    }

    @Override
    public Integer visit(Declaration declaration) {
        return declaration.getLabelIn();
    }

    @Override
    public Integer visit(Statements statements) {
        return statements.getStatement().accept(this);
    }

}
