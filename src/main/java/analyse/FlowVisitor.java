package analyse;

import ast.*;
import support.Pair;

import java.util.*;

/**
 * Donne le flow avant du programme
 */

public class FlowVisitor extends DefaultVisitor<Set<Pair<Integer, Integer>>> {

    InitVisitor initVisitor;
    FinalVisitor finalVisitor;
    Map<String, Pair<Integer, Integer>> proc;

    public FlowVisitor(Set<Pair<Integer, Integer>> defaultValue) {
        super(defaultValue);
        initVisitor = new InitVisitor(null);
        finalVisitor = new FinalVisitor(null);
        proc = new HashMap<>();
    }

    @Override
    public Set<Pair<Integer, Integer>> visit(If sif) {
        Set<Pair<Integer, Integer>> flow = sif.getIfBlock().accept(this);
        flow.add(new Pair<>(sif.getLabel(), sif.getIfBlock().accept(initVisitor)));
        if (sif.getElseBlock().isPresent()) {
            flow.add(new Pair<>(sif.getLabel(), sif.getElseBlock().get().accept(initVisitor)));
            flow.addAll(sif.getElseBlock().get().accept(this));
        }
        return flow;
    }

    @Override
    public Set<Pair<Integer, Integer>> visit(While swhile) {
        Set<Pair<Integer, Integer>> flow = swhile.getBlock().accept(this);
        flow.add(new Pair<>(swhile.getLabel(), swhile.getBlock().accept(initVisitor)));
        for (Integer f : swhile.getBlock().accept(finalVisitor)) {
            flow.add(new Pair<>(f, swhile.getLabel()));
        }
        return flow;
    }

    @Override
    public Set<Pair<Integer, Integer>> visit(Call call) {
        Pair<Integer, Integer> labelProc = proc.get(call.getIdentifier().getName());
        Set<Pair<Integer, Integer>> flow = new HashSet<>();
        flow.add(new Pair<>(call.getLabel(), labelProc.getFst()));
        flow.add(new Pair<>(labelProc.getSnd(), call.getLabelOut()));
        return flow;
    }


    @Override
    public Set<Pair<Integer, Integer>> visit(Program program) {
        Set<Pair<Integer, Integer>> flow = new HashSet<>();
        for (Declaration declaration : program.getDeclarations())
            flow.addAll(declaration.accept(this));
        flow.addAll(program.getStatements().accept(this));
        return flow;
    }


    @Override
    public Set<Pair<Integer, Integer>> visit(BlockOnly blockOnly) {
        return blockOnly.getStatement().accept(this);
    }

    @Override
    public Set<Pair<Integer, Integer>> visit(BlockParenthesis blockParenthesis) {
        return blockParenthesis.getStatements().accept(this);
    }

    @Override
    public Set<Pair<Integer, Integer>> visit(Declaration declaration) {
        proc.put(declaration.getName().getName(), new Pair<>(declaration.getLabelIn(), declaration.getLabelOut()));

        Set<Pair<Integer, Integer>> flow = new HashSet<>();
        flow.add(new Pair<>(declaration.getLabelIn(), declaration.getStatements().accept(initVisitor)));
        flow.addAll(declaration.getStatements().accept(this));

        for (Integer f : declaration.getStatements().accept(finalVisitor)) {
            flow.add(new Pair<>(f, declaration.getLabelOut()));
        }
        return flow;
    }

    @Override
    public Set<Pair<Integer, Integer>> visit(Statements statements) {
        Set<Pair<Integer, Integer>> flow = statements.getStatement().accept(this);
        List<Integer> precs = statements.getStatement().accept(finalVisitor);

        for (Integer prec : precs) {
            for (int i = 0; i < statements.getStatements().size(); i++) {
                if (i == 0) {
                    flow.add(new Pair<>(prec, statements.getStatements().get(0).accept(initVisitor)));
                }
                flow.addAll(statements.getStatements().get(i).accept(this));
            }
        }
        return flow;
    }


}
