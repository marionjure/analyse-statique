package analyse;

import ast.Program;
import support.Pair;

import java.util.HashSet;
import java.util.Set;

/**
 * Donne le flow arrière du programme
 */
public class FlowRVisitor extends FlowVisitor {

    Set<Pair<Integer, Integer>> flowR;

    public FlowRVisitor(Program program) {
        super(new HashSet<>());
        flowR = new HashSet<>();
        Set<Pair<Integer, Integer>> flow = program.accept(this);
        for (Pair pair : flow) {
            flowR.add(new Pair<>((Integer) pair.getSnd(), (Integer) pair.getFst()));
        }
    }

    public Set<Pair<Integer, Integer>> getFlowR() {
        return flowR;
    }
}
