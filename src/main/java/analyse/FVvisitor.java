package analyse;

import ast.*;

import java.util.HashSet;
import java.util.Set;

/**
 * Donne les variables libres du programme
 */

public class FVvisitor extends DefaultVisitor<Set<Identifier>> {
    public FVvisitor(Set<Identifier> defaultValue) {
        super(defaultValue);
    }

    @Override
    public Set<Identifier> visit(Affectation affectation) {
        Set<Identifier> l = new HashSet<>();
        l.add(affectation.getIdentifier());
        l.addAll(affectation.getAexpression().accept(this));
        return l;
    }

    @Override
    public Set<Identifier> visit(Program program) {
        Set<Identifier> l = new HashSet<>();
        for (Declaration declaration : program.getDeclarations())
            l.addAll(declaration.accept(this));
        l.addAll(program.getStatements().accept(this));
        return l;
    }

    @Override
    public Set<Identifier> visit(Declaration declaration) {
        return declaration.getStatements().accept(this);
    }

    @Override
    public Set<Identifier> visit(BlockOnly blockOnly) {
        return blockOnly.getStatement().accept(this);
    }

    @Override
    public Set<Identifier> visit(BlockParenthesis blockParenthesis) {
        return blockParenthesis.getStatements().accept(this);
    }

    @Override
    public Set<Identifier> visit(If sif) {
        Set<Identifier> l = new HashSet<>();
        l.addAll(sif.getBexpression().accept(this));
        l.addAll(sif.getIfBlock().accept(this));
        if (sif.getElseBlock().isPresent())
            l.addAll(sif.getElseBlock().get().accept(this));
        return l;
    }

    @Override
    public Set<Identifier> visit(While swhile) {
        Set<Identifier> l = new HashSet<>();
        l.addAll(swhile.getBexpression().accept(this));
        l.addAll(swhile.getBlock().accept(this));
        return l;
    }

    @Override
    public Set<Identifier> visit(Statements statements) {
        Set<Identifier> l = new HashSet<>();
        l.addAll(statements.getStatement().accept(this));
        for (Statements statement : statements.getStatements()) {
            l.addAll(statement.accept(this));
        }
        return l;
    }

    @Override
    public Set<Identifier> visit(BExpParenthesis bExpParenthesis) {
        return bExpParenthesis.getBexpression().accept(this);
    }

    @Override
    public Set<Identifier> visit(BExpNot bExpNot) {
        return bExpNot.getExpression().accept(this);
    }

    @Override
    public Set<Identifier> visit(BExpBinary bExpBinary) {
        Set<Identifier> l = new HashSet<>();
        l.addAll(bExpBinary.getLeft().accept(this));
        l.addAll(bExpBinary.getRight().accept(this));
        return l;
    }

    @Override
    public Set<Identifier> visit(AExpBinary aExpBinary) {
        Set<Identifier> l = new HashSet<>();
        l.addAll(aExpBinary.getAexpressionLeft().accept(this));
        l.addAll(aExpBinary.getAexpressionRight().accept(this));
        return l;
    }

    @Override
    public Set<Identifier> visit(Call call) {
        return call.getlAexpression().accept(this);
    }

    @Override
    public Set<Identifier> visit(LAexpression lAexpression) {
        Set<Identifier> l = new HashSet<>();
        for (Aexpression aexpression : lAexpression.getAexpressionList()) {
            l.addAll(aexpression.accept(this));
        }
        return l;
    }

    @Override
    public Set<Identifier> visit(Identifier identifier) {
        Set<Identifier> l = new HashSet<>();
        l.add(identifier);
        return l;
    }

    public Set<Identifier> visit(Aexpression aexpression) {
        return aexpression.accept(this);
    }
}

