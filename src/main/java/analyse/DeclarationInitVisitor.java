package analyse;

import ast.*;
import support.Pair;

import java.util.HashSet;
import java.util.Set;

/**
 * Initialisation de l'information initiale de l'analyse RD
 */

public class DeclarationInitVisitor extends DefaultVisitor<Set<Pair<Identifier, Integer>>> {
    public DeclarationInitVisitor() {
        super(new HashSet<>());
    }

    @Override
    public Set<Pair<Identifier, Integer>> visit(LDeclVariables lDeclVariables) {
        Set<Pair<Identifier, Integer>> l = new HashSet<>();
        l.addAll(lDeclVariables.getDeclVariables().accept(this));
        for (LDeclVariables declVariables : lDeclVariables.getlDeclVariables()) {
            l.addAll(declVariables.getDeclVariables().accept(this));
        }
        return l;
    }

    @Override
    public Set<Pair<Identifier, Integer>> visit(LDeclIdent lDeclIdent) {
        Set<Pair<Identifier, Integer>> l = new HashSet<>();
        for (Identifier identifier : lDeclIdent.getParameters()) {
            l.add(new Pair<>(identifier, null));
        }
        return l;
    }

    @Override
    public Set<Pair<Identifier, Integer>> visit(Declaration declaration) {
        Set<Pair<Identifier, Integer>> l = new HashSet<>();
        if (declaration.getNameRes().isPresent()) {
            l.add(new Pair<>(declaration.getNameRes().get(), null));
        }
        l.addAll(declaration.getlDeclIdent().accept(this));
        l.addAll(declaration.getStatements().accept(this));
        return l;
    }

    @Override
    public Set<Pair<Identifier, Integer>> visit(DeclVariables declVariables) {
        return declVariables.getlIdentifier().accept(this);
    }

    @Override
    public Set<Pair<Identifier, Integer>> visit(LIdentifier lIdentifier) {
        Set<Pair<Identifier, Integer>> l = new HashSet<>();
        for (Identifier identifier : lIdentifier.getIdentifiers()) {
            l.add(new Pair<>(identifier, null));
        }
        return l;
    }

    @Override
    public Set<Pair<Identifier, Integer>> visit(Program program) {
        Set<Pair<Identifier, Integer>> l = new HashSet<>();
        l.addAll(program.getlDeclVariables().accept(this));
        return l;
    }

}
