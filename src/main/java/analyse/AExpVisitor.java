package analyse;

import ast.*;

import java.util.HashSet;
import java.util.Set;

/**
 * Récupérer les sous-expressions d'un programme
 */

public class AExpVisitor extends DefaultVisitor<Set<Aexpression>> {
    private final FVvisitor fVvisitor;

    public AExpVisitor(Set<Aexpression> defaultValue) {
        super(defaultValue);
        this.fVvisitor = new FVvisitor(new HashSet<>());
    }

    @Override
    public Set<Aexpression> visit(Affectation affectation) {
        Set<Aexpression> l = new HashSet<>();
        if (affectation.getAexpression().accept(fVvisitor).size() != 0 && affectation.getAexpression().getClass().getName().compareTo("ast.AExpIdent") != 0) {
            l.add(affectation.getAexpression());
        }
        return l;
    }

    @Override
    public Set<Aexpression> visit(Program program) {
        Set<Aexpression> l = new HashSet<>();
        for (Declaration declaration : program.getDeclarations())
            l.addAll(declaration.accept(this));
        l.addAll(program.getStatements().accept(this));
        return l;
    }

    @Override
    public Set<Aexpression> visit(Declaration declaration) {
        return declaration.getStatements().accept(this);
    }

    @Override
    public Set<Aexpression> visit(BlockOnly blockOnly) {
        return blockOnly.getStatement().accept(this);
    }

    @Override
    public Set<Aexpression> visit(BlockParenthesis blockParenthesis) {
        return blockParenthesis.getStatements().accept(this);
    }

    @Override
    public Set<Aexpression> visit(If sif) {
        Set<Aexpression> l = new HashSet<>();
        l.addAll(sif.getBexpression().accept(this));
        l.addAll(sif.getIfBlock().accept(this));
        if (sif.getElseBlock().isPresent())
            l.addAll(sif.getElseBlock().get().accept(this));
        return l;
    }

    @Override
    public Set<Aexpression> visit(While swhile) {
        Set<Aexpression> l = new HashSet<>();
        l.addAll(swhile.getBexpression().accept(this));
        l.addAll(swhile.getBlock().accept(this));
        return l;
    }

    @Override
    public Set<Aexpression> visit(Statements statements) {
        Set<Aexpression> l = new HashSet<>();
        l.addAll(statements.getStatement().accept(this));
        for (Statements statement : statements.getStatements()) {
            l.addAll(statement.accept(this));
        }
        return l;
    }

    @Override
    public Set<Aexpression> visit(BExpNot bExpNot) {
        return bExpNot.getExpression().accept(this);
    }

    @Override
    public Set<Aexpression> visit(BExpBinary bExpBinary) {
        Set<Aexpression> l = new HashSet<>();
        if (bExpBinary.getLeft().accept(fVvisitor).size() != 0 && bExpBinary.getLeft().getClass().getName().compareTo("ast.AExpIdent") != 0) {
            l.add(bExpBinary.getLeft());
        }
        if (bExpBinary.getRight().accept(fVvisitor).size() != 0 && bExpBinary.getRight().getClass().getName().compareTo("ast.AExpIdent") != 0) {
            l.add(bExpBinary.getRight());
        }
        return l;
    }

    @Override
    public Set<Aexpression> visit(Call call) {
        return call.getlAexpression().accept(this);
    }

    @Override
    public Set<Aexpression> visit(LAexpression lAexpression) {
        Set<Aexpression> l = new HashSet<>();
        for (Aexpression aexpression : lAexpression.getAexpressionList()) {
            l.add(aexpression);
        }
        return l;
    }
}
