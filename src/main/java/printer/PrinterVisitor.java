package printer;

import ast.*;
import ast.Boolean;

import java.util.ArrayList;
import java.util.List;

import static support.ListTools.singleton;

import static support.ListTools.*;

/**
 * Classe pour afficher le programme avec les labels pour chaque instruction
 */
public class PrinterVisitor implements Visitor<List<String>> {
    private static String spaceAround(String s) {
        return " " + s + " ";
    }

    private static String parenthesis(String s) {
        return "(" + s + ")";
    }
    private static String semicolon(String s) {
        return s + ";";
    }
    private static String label(String s){ return "[" + s + "]";}

    @Override
    public List<String> visit(Program program) {

        String name =
                program.getName().isEmpty() ? " " :
                        program.getName().get().getName();
        List<String> listing = new ArrayList<>(singleton("program " + name));
        for (Declaration declaration: program.getDeclarations())
            listing.addAll(declaration.accept(this));
        listing.add("begin");
        listing.addAll(program.getlDeclVariables().accept(this));
        listing.addAll(program.getStatements().accept(this));
        listing.add("end");
        return listing;
    }

    @Override
    public List<String> visit(AExpIdent aExpIdent) {
        return singleton(aExpIdent.getIdentifier().getName());
    }

    @Override
    public List<String> visit(AExpConst aExpConst) {
        return singleton(String.valueOf(aExpConst.getConstant().getValue()));
    }

    @Override
    public List<String> visit(AExpBinary aExpBinary) {
        String left = stringFlatten(aExpBinary.getAexpressionLeft().accept(this));
        String right = stringFlatten(aExpBinary.getAexpressionRight().accept(this));
        String operator = aExpBinary.getBinaryOp().toString();
        return singleton(left + spaceAround(operator) + right);
    }

    @Override
    public List<String> visit(AExpOpposite aExpOpposite) {
        return singleton("-" + stringFlatten(aExpOpposite.getAexpression().accept(this)));
    }

    @Override
    public List<String> visit(AExpParenthesis aExpParenthesis) {
        return singleton(parenthesis(stringFlatten(aExpParenthesis.getAexpression().accept(this))));
    }

    @Override
    public List<String> visit(BExpTrue bExpTrue) {
        return singleton("true");
    }

    @Override
    public List<String> visit(BExpFalse bExpFalse) {
        return singleton("false");
    }

    @Override
    public List<String> visit(BExpBinary bExpBinary) {
        String left = stringFlatten(bExpBinary.getLeft().accept(this));
        String operator = bExpBinary.getOperator().toString();
        String right = stringFlatten(bExpBinary.getRight().accept(this));
        return singleton(left + spaceAround(operator) + right);
    }

    @Override
    public List<String> visit(BExpNot bExpNot) {
        String operator = bExpNot.getOperation().toString();
        String expression = stringFlatten(bExpNot.getExpression().accept(this));
        return singleton(operator + expression);
    }

    @Override
    public List<String> visit(BExpParenthesis bExpParenthesis) {
        return singleton(parenthesis(stringFlatten(bExpParenthesis.getBexpression().accept(this))));
    }

    @Override
    public List<String> visit(BlockOnly blockOnly) {
        return blockOnly.getStatement().accept(this);
    }

    @Override
    public List<String> visit(BlockParenthesis blockParenthesis) {
        List<String> listing = new ArrayList<>();
        listing.add("(");
        listing.addAll(blockParenthesis.getStatements().accept(this));
        listing.add(")");
        return listing;
    }

    @Override
    public List<String> visit(Declaration declaration) {
        String res =
                declaration.getTypeRes().isEmpty() || declaration.getNameRes().isEmpty() ? " " :
                        ", res " + stringFlatten(declaration.getTypeRes().get().accept(this))
                                 + stringFlatten(declaration.getNameRes().get().accept(this));
        List<String> listing = new ArrayList<>(singleton("proc " +
                declaration.getName().getName() +
                parenthesis(stringFlatten(declaration.getlDeclIdent().accept(this))+ res)));
        listing.add("begin" + label(String.valueOf(declaration.getLabelIn())));
        listing.addAll(declaration.getStatements().accept(this));
        listing.add("end" + label(String.valueOf(declaration.getLabelOut())));
        return listing;
    }

    @Override
    public List<String> visit(LDeclIdent lDeclIdent) {
        List<String> listing = new ArrayList<>();
        for (int i = 0 ; i < lDeclIdent.getParameters().size(); i++){
            listing.add(stringFlatten(lDeclIdent.getTypes().get(i).accept(this)) + " "
                    + stringFlatten(lDeclIdent.getParameters().get(i).accept(this)));
        }
        return singleton(commaSeparatedList(listing));
    }

    @Override
    public List<String> visit(LDeclVariables lDeclVariables) {
        List<String> listing = new ArrayList<>(lDeclVariables.getDeclVariables().accept(this));
        for (LDeclVariables lDeclVariables1 :lDeclVariables.getlDeclVariables())
            listing.addAll(lDeclVariables1.accept(this));
        return listing;
    }

    @Override
    public List<String> visit(DeclVariables declVariables) {
        return singleton(semicolon(stringFlatten(declVariables.getType().accept(this))
                + stringFlatten(declVariables.getlIdentifier().accept(this))));
    }

    @Override
    public List<String> visit(LIdentifier lIdentifier) {
        List<String> listing = new ArrayList<>();
        List<Identifier> identifiers = lIdentifier.getIdentifiers();
        for (Identifier identifier : identifiers)
            listing.addAll(identifier.accept(this));
        return singleton(commaSeparatedList(listing));
    }

    @Override
    public List<String> visit(Statement statement) {
        return statement.accept(this);
    }

    @Override
    public List<String> visit(Statements statements) {
        List<String> listing = new ArrayList<>(statements.getStatement().accept(this));
        for (Statements statements1 : statements.getStatements())
            listing.addAll(statements1.accept(this));
        return listing;
    }


    @Override
    public List<String> visit(LAexpression lAexpression) {
        List<String> listing = new ArrayList<>();
        List<Aexpression> aexpressionList = lAexpression.getAexpressionList();
        for (Aexpression aexpression: aexpressionList)
            listing.addAll(aexpression.accept(this));
        return listing;
    }

    @Override
    public List<String> visit(Identifier identifier) {
        return singleton(identifier.getName());
    }

    @Override
    public List<String> visit(Constant constant) {
        return singleton(String.valueOf(constant.getValue()));
    }

    @Override
    public List<String> visit(End end) {
        return  singleton("");
    }

    @Override
    public List<String> visit(Begin begin) {
        return  singleton("");
    }

    @Override
    public List<String> visit(Skip skip) {
        return singleton("skip" + label(String.valueOf(skip.getLabel())));
    }

    @Override
    public List<String> visit(Affectation affectation) {
        return singleton(semicolon(stringFlatten(affectation.getIdentifier().accept(this))
                + " := " + stringFlatten(affectation.getAexpression().accept(this))
                + label(String.valueOf(affectation.getLabel()))));
    }

    @Override
    public List<String> visit(If sif) {
        List<String> listing = new ArrayList<>();
        String cdtIf = "if" + spaceAround(stringFlatten(sif.getBexpression().accept(this)))
                            + label(String.valueOf(sif.getLabel()));
        String cdtThen = "then";
        String cdtElse = "else";
        listing.add(cdtIf);
        listing.add(cdtThen);
        listing.addAll(sif.getIfBlock().accept(this));
        if (sif.getElseBlock().isPresent()) {
            listing.add(cdtElse);
            listing.addAll(sif.getElseBlock().get().accept(this));
        }
        return listing;
    }

    @Override
    public List<String> visit(While swhile) {
        List<String> listing = new ArrayList<>();
        String condition = "while" + spaceAround(stringFlatten(swhile.getBexpression().accept(this)))
                                   + label(String.valueOf(swhile.getLabel())) + "do";
        List<String> body = swhile.getBlock().accept(this);
        listing.add(condition);
        listing.addAll(body);
        return listing;
    }

    @Override
    public List<String> visit(Call call) {
        String procCall = semicolon("call " + stringFlatten(call.getIdentifier().accept(this))
                + parenthesis(commaSeparatedList(call.getlAexpression().accept(this))))
                + label(String.valueOf(call.getLabel()))
                + label(String.valueOf(call.getLabelOut()));
        return singleton(procCall);
    }

    @Override
    public List<String> visit(Int tint) {
        return singleton("int ");
    }

    @Override
    public List<String> visit(Boolean tboolean) {
        return singleton("boolean ");
    }
}
