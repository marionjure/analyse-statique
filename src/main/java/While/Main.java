package While;

import analyse.DeclarationInitVisitor;
import ast.Build;
import ast.Expression;
import ast.Identifier;
import framework.*;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import parser.WhileLexer;
import parser.WhileParser;
import support.Pair;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.HashSet;

public class Main {
    private enum ErrorCode {
        SUCCESS,
        NO_FILE_NAME,
        FILE_NOT_FOUND,
        SYNTAX_ERROR,
        NO_ANALYSE_NAME
    }

    private static void exitWithCode(ErrorCode code){
        System.exit(code.ordinal());
    }


    private static ast.Program buildAst(ParseTree parseTree){
        ast.Build builder = new Build();
        return (ast.Program) parseTree.accept(builder);
     }

     private static InputStream getInputStream(String fileName){
        try {
            if (fileName != null)
                return new FileInputStream(fileName);
            } catch (FileNotFoundException e) {
                exitWithCode(ErrorCode.FILE_NOT_FOUND);
            }
        return System.in;
     }

     private static ParseTree parse(InputStream inputStream) throws IOException {
        CharStream input = CharStreams.fromStream(inputStream);
        // Creation of the lexer for pico programs
        WhileLexer lexer = new WhileLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        // Creation of the parser for pico programs
        WhileParser parser = new WhileParser(tokens);
        // Parse the input: the result is a parse tree
        ParseTree tree = parser.program();
        if (parser.getNumberOfSyntaxErrors() != 0)
            exitWithCode(ErrorCode.SYNTAX_ERROR);
        return tree;
     }

    private static Path changeExtension(Path path, String oldExt, String newExt) {
        PathMatcher pm = FileSystems.getDefault()
                .getPathMatcher("glob:*" + oldExt);
        if (pm.matches(path.getFileName())) {
            String nameWithExtension = path.getFileName().toString();
            int endIndex = nameWithExtension.length() - oldExt.length();
            String name = nameWithExtension.substring(0, endIndex);
            if (path.getParent() != null)
                return path.getParent().resolve(name + newExt);
            else
                return FileSystems.getDefault().getPath(name + newExt);
        }
        return path;
    }

    public static void main(String[] arguments) throws IOException {
        if (arguments.length == 0)
            // No name given to the command line
            exitWithCode(ErrorCode.NO_FILE_NAME);
        if (arguments.length == 1)
            exitWithCode(ErrorCode.NO_ANALYSE_NAME);
        String fileName = arguments[0];
        String analyse = arguments[1];

        InputStream inputStream = getInputStream(fileName);
        ParseTree parseTree = parse(inputStream);
        ast.Program program = buildAst(parseTree);

        if (analyse.toUpperCase().compareTo("AE") == 0){
            MFP<Expression> mfp = new MFP<>(program,false,true,new AEVisitor(),new HashSet<>(),true);
        }
        else if (analyse.toUpperCase().compareTo("RD") == 0){
           MFP<Pair<Identifier,Integer>> mfp = new MFP<>(program,true,true,new RDVisitor(),program.accept(new DeclarationInitVisitor()),true);
        }
        else if (analyse.toUpperCase().compareTo("LV") == 0){
             MFP<Identifier> mfpLV = new MFP<>(program,true,false,new LVVisitor(),new HashSet<>(),true);
        }
        else if (analyse.toUpperCase().compareTo("VB") == 0){
             MFP<Expression> mfp = new MFP<>(program,false,false,new VBVisitor(), new HashSet<>(), true);
        }
        else {
            System.out.println("Erreur dans le nom de l'analyse");
        }
        exitWithCode(ErrorCode.SUCCESS);
    }
}


