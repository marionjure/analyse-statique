package framework;

import analyse.*;
import ast.*;
import support.Pair;

import java.util.*;

/**
 * La classe pour la fonction de transfert de l'analyse RD
 */
public class RDVisitor extends DefaultVisitor<Set<Pair<Identifier,Integer>>> implements AnalyseVisitor<Pair<Identifier,Integer>> {

    /**
     *  Le visitor permet de calculer les labels où les variables sont déclarées.
     */
    private DeclarationVisitor declarationVisitor;
    /**
     * Pour chaque variable, donne les labels où elles sont déclarées.
     * ex : {x : {(x,1),(x,?)}}
     */
    private Map<Identifier,Set<Pair<Identifier,Integer>>> kill;

    /**
     * L'analyse pour chaque labels
     */
    private Map<Integer, Set<Pair<Identifier,Integer>>> analyse;
    /**
     * le programme
     */
    private  Program program;
    /**
     * Sauvegarder de kill
     */
    private Map<Integer,Map<Identifier,Set<Pair<Identifier,Integer>>>> killSave;

    /**
     * Constructeur
     */
    public RDVisitor() {
        super(new HashSet<>());
        this.declarationVisitor = new DeclarationVisitor();
        this.killSave = new HashMap<>();
    }

    /**
     * Modifier le programme
     * @param program nouveau programme
     */
    @Override
    public void setProgram(Program program){
        this.program= program;
        this.kill = this.program.accept(declarationVisitor);
    }

    /**
     * Modifier l'analyse
     * @param analyse  nouvelle analyse
     */
    @Override
    public void setAnalyse(Map<Integer, Set<Pair<Identifier,Integer>>> analyse) {
        this.analyse = analyse;
    }

    // Fonction kill et gen pour chaque instructions

    @Override
    public Set<Pair<Identifier,Integer>> visit2(Call call) {
        this.kill = killSave.get(call.getLabelOut());
        Set<Pair<Identifier,Integer>> set = new HashSet<>(analyse.get(call.getLabel()));
        for(Declaration d : program.getDeclarations()){
            if (d.getName().equals(call.getIdentifier())){
                if (d.getNameRes().isPresent()){
                    List<Aexpression> paramsCall = call.getlAexpression().getAexpressionList();
                    Identifier resCall = ((AExpIdent)paramsCall.get(paramsCall.size()-1)).getIdentifier();
                    set.removeAll(kill.get(resCall));
                    set.add(new Pair<>(resCall,call.getLabelOut()));
                }
            }
        }
        return set;
    }

    @Override
    public Set<Pair<Identifier,Integer>> visit(Node node) {
        return node.accept(this);
    }

    @Override
    public Set<Pair<Identifier,Integer>> visit(Call call) {
        killSave.put(call.getLabelOut(),this.kill);
        return analyse.get(call.getLabel());
    }

    @Override
    public Set<Pair<Identifier,Integer>> visit(Begin Begin) {
        // Sauvegarder le contexte
        Set<Pair<Identifier,Integer>> l = new HashSet<>();
        for (Declaration declaration : program.getDeclarations()){
            if (declaration.getLabelIn() == Begin.getLabel()){
                this.kill = declarationVisitor.visit(declaration,program);
                l.addAll(declaration.accept(new DeclarationInitVisitor()));
            }
        }
        return l;
    }

    @Override
    public Set<Pair<Identifier,Integer>> visit(End end) {
        return analyse.get(end.getLabel());
    }

    @Override
    public Set<Pair<Identifier,Integer>> visit(Skip skip) {
        return analyse.get(skip.getLabel());
    }

    @Override
    public Set<Pair<Identifier,Integer>> visit(BExpParenthesis bExpParenthesis) {
        return analyse.get(bExpParenthesis.getLabel());
    }

    @Override
    public Set<Pair<Identifier,Integer>> visit(BExpBinary bExpBinary) {
        return analyse.get(bExpBinary.getLabel());
    }

    @Override
    public Set<Pair<Identifier,Integer>> visit(BExpNot bExpNot) {
        return analyse.get(bExpNot.getLabel());
    }

    @Override
    public Set<Pair<Identifier,Integer>> visit(Affectation affectation) {
        Set<Pair<Identifier,Integer>> set = new HashSet<>(analyse.get(affectation.getLabel()));
        // kill
        set.removeAll(kill.get(affectation.getIdentifier()));
        // gen
        set.add(new Pair<>(affectation.getIdentifier(),affectation.getLabel()));
        return set;
    }


}
