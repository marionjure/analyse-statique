package framework;

import analyse.ChangerValeur2Visitor;
import ast.*;
import support.Pair;

import java.util.*;


/**
 * La classe pour la fonction de transfert de l'analyse pour déterminer la valeur des variables
 */
public class DDVisitor extends DefaultVisitor<Set<Pair<Identifier,Expression>>> implements AnalyseVisitor<Pair<Identifier,Expression>> {
    /**
     *  L'analyse pour chaque label
     */
    private Map<Integer, Set<Pair<Identifier,Expression>>> analyse;
    /**
     * Le programme
     */
    private  Program program;

    /**
     * Constructeur
     */
    public  DDVisitor(){
        super(new HashSet<>());
    }

    /**
     * Modifier le programme
     * @param program nouveau programme
     */
    @Override
    public void setProgram(Program program){
        this.program = program;
    }

    /**
     *  Modifier l'analyse
     * @param analyse: nouvelle analyse
     */
    @Override
    public void setAnalyse(Map<Integer, Set<Pair<Identifier,Expression>>> analyse) {
        this.analyse = analyse;
    }

    // Fonction kill et gen pour chaque instructions

    @Override
    public Set<Pair<Identifier,Expression>> visit2(Call call) {
        Set<Pair<Identifier,Expression>> set;

        if (analyse.get(call.getLabel()) != null)
            set = analyse.get(call.getLabel());
        else
            set = new HashSet<>();

        Declaration declaration = null;
        for (Declaration d: program.getDeclarations()){
            if (d.getName().equals(call.getIdentifier()))
                declaration = d;
        }

        // Modifie le contexte du programme après la sortie de la procédure.
        if (declaration.getNameRes().isPresent()){
            List<Aexpression> paramsCall = call.getlAexpression().getAexpressionList();
            Identifier resCall = ((AExpIdent)paramsCall.get(paramsCall.size()-1)).getIdentifier();
            Set<Pair<Identifier,Expression>> analyseCall = analyse.get(call.getLabelOut());
            Pair<Identifier,Expression> pair = null;
            // Sélectionner la variable du résultat du call si elle est dans le contexte
            for (var pair1 : set){
                if (pair1.getFst().equals(resCall)){
                    pair=pair1;
                }
            }
            // La supprimer du contexte
            if (!Objects.isNull(pair)){
                set.remove(pair);
            }
            // Ajouter dans le contexte la variable du résultat
            for (var pair1 : analyseCall){
                if (pair1.getFst().equals(declaration.getNameRes().get())){
                    set.add(new Pair<>(resCall,pair1.getSnd()));
                }
            }
        }
        return set;
    }

    @Override
    public Set<Pair<Identifier,Expression>> visit(Call call) {
        Declaration declaration = null;
        for (Declaration d: program.getDeclarations()){
            if (d.getName().equals(call.getIdentifier()))
                declaration = d;
        }

        //Changer le contexte selon la procédure
        Set<Pair<Identifier,Expression>> set = new HashSet<>();
        List<Identifier> paramsProcedure = declaration.getlDeclIdent().getParameters();
        List<Aexpression> paramCall = call.getlAexpression().getAexpressionList();
        for (int i = 0 ; i <paramsProcedure.size(); i++){
            set.add(new Pair<>(paramsProcedure.get(i),paramCall.get(i)));
        }

        return set;
    }

    @Override
    public Set<Pair<Identifier,Expression>> visit(Node node) {
        return node.accept(this);
    }

    @Override
    public Set<Pair<Identifier,Expression>> visit(Skip skip) {
        return analyse.get(skip.getLabel());
    }

    @Override
    public Set<Pair<Identifier,Expression>> visit(End end) {
        return analyse.get(end.getLabel());
    }

    @Override
    public Set<Pair<Identifier,Expression>> visit(Begin begin) {
        return new HashSet<>(analyse.get(begin.getLabel()));
    }

    @Override
    public Set<Pair<Identifier,Expression>> visit(BExpParenthesis bExpParenthesis) {
        return new HashSet<>(analyse.get(bExpParenthesis.getLabel()));
    }

    @Override
    public Set<Pair<Identifier,Expression>> visit(BExpBinary bExpBinary) {
        return new HashSet<>(analyse.get(bExpBinary.getLabel()));
    }

    @Override
    public Set<Pair<Identifier,Expression>> visit(Affectation affectation) {
        Set<Pair<Identifier,Expression>> set = new HashSet<>(analyse.get(affectation.getLabel()));
        Map<Identifier,Aexpression> newSet = new HashMap<>();

        Pair<Identifier,Expression> valeur = null;
        for (var e : set){
            newSet.put(e.getFst(),(Aexpression) e.getSnd());
            if (e.getFst().equals(affectation.getIdentifier())){
                valeur = e;
            }
        }
        if (!Objects.isNull(valeur)){
            set.remove(valeur);
        }
        Expression expression = (Expression) affectation.getAexpression().accept(new ChangerValeur2Visitor(newSet));
        set.add(new Pair<>(affectation.getIdentifier(),expression));
        return set;
    }
}





