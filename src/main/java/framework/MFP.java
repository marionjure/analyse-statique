package framework;

import analyse.*;
import ast.*;
import printer.PrinterVisitor;
import support.Pair;
import support.Void;

import java.util.*;


/**
 * Classe pour l'algorithme MFP
 * @param <T>
 */
public class MFP<T> {

    /**
     * Le flow arrière ou avant
     */
    private Set<Pair<Integer, Integer>> F;
    /**
     *  Les labels initiaux ou finaux
     */
    private List<Integer> E;
    /**
     * l’information d’analyse initiale ou finale
     */
    private Set<T> i;
    /**
     * L'opération jointure
     *      true = union
     *      false = intersection
     */
    private boolean jointure;

    /**
     * le flow des instructions
     *      true = flow avant
     *      false = flow arriere
     */
    private boolean flow;

    /**
     * Les blocs du programme
     */
    private List<Node> bloc;
    /**
     * L'ensemble des labels
     */
    private ArrayList<Integer> labels;
    /**
     * L'analyse pour chaque label
     */
    private Map<Integer,Set<T>> analyse;
    /**
     * La classe qui représente la fonction de transfert
     */
    private AnalyseVisitor analyseVisitor;
    /**
     * Affichier les logs
     */
    private boolean isLog;
    /**
     * le type de flow
     */
    private boolean isFlow;

    /**
     * Constructeur
     * @param program : le programme à analyser
     * @param isUnion : le type de jointure:
     *                 true = union
     *                 false = intersection
     * @param isFlow : le type de flow
     *                true = flow
     *               false = flowR
     * @param analyseVisitor : la classe de la fonction de transfert
     * @param i : l’information d’analyse initiale ou finale
     */
    public MFP(Program program, boolean isUnion, boolean isFlow, AnalyseVisitor analyseVisitor, Set<T> i, boolean isLog) {
        // Ajouter les labels au programme
        LabelVisitor labelVisitor = new LabelVisitor(new Void());
        labelVisitor.visit(program);
        this.labels = labelVisitor.getList();
        this.isFlow =isFlow;
        // initialiser paramètres de algo mfp
        if (isFlow){
            this.F = new FlowVisitor(new HashSet<>()).visit(program);
            this.E = List.of(new InitVisitor(null).visit(program));
        }
        else {
            this.F = new FlowRVisitor(program).getFlowR();
            this.E = new FinalVisitor(null).visit(program);
        }
        this.i = i;
        this.jointure = isUnion;
        this.flow = isFlow;
        this.bloc =  new BlockVisitor().visit(program);
        this.analyse = new HashMap<>();

        // initialiser la fonction de transfert
        this.analyseVisitor = analyseVisitor;
        this.analyseVisitor.setAnalyse(analyse);
        this.analyseVisitor.setProgram(program);

        this.isLog = isLog;
        //Print programme
        if (isLog) {
            System.out.println(this);
            this.printProgram(program);
        }

        // Lancer algo
        if (isLog)
            System.out.println("Tableau d'analyse :\n");
        this.algoMfp();

    }

    /**
     * Donne le block à partir d'un label
     * @param label : label
     * @return un block
     */
    public Node getBlock(Integer label) {
        for (Node node : bloc) {
            if(node.getClass().getName().compareTo("ast.Call")==0){
                Call call=(Call) node;
                if(call.getLabelOut() == label || call.getLabel() == label){
                    return node;
                }
            }
            else if (node.getLabel() == label){
                return node;
            }
        }
        return null;
    }

    /**
     * Algo MFP
     */
    public void algoMfp(){
        ArrayList<Pair<Integer, Integer>> W = new ArrayList<>();
        for (Integer l : E){
            W.addAll(getLabel(l));
        }
        for (Integer l : labels){
            if (E.contains(l)){
                analyse.put(l,i);
            }
            else{
                analyse.put(l,null);
            }
        }
        // Affichage de l'initialisation

        while (W.size() != 0){
            Pair<Integer,Integer> pair_cur = W.remove(0);
            if (isLog)
                System.out.println(pair_cur+"  "+analyse);
            Node block = getBlock(pair_cur.getFst());
            Set<T> setL;
            if(block.getClass().getName().compareTo("ast.Call") == 0 && (
                    (flow && ((Call)block).getLabelOut() == pair_cur.getFst()) // flow avant
                ||  (!flow && block.getLabel() == pair_cur.getFst()))          // flow arriere
            ){
                setL = analyseVisitor.visit2((Call)block);
            }
            else {
                setL = analyseVisitor.visit(block);
            }
            if (checkRelationOrder(setL,analyse.get(pair_cur.getSnd()))){
                analyse.put(pair_cur.getSnd(), operationJointure(setL,analyse.get(pair_cur.getSnd())));
                W.addAll(0, getLabel(pair_cur.getSnd()));
            }
        }
        // analyse final
        if (isLog)
            System.out.println("\nRésultat final : " + analyse);

    }

    /**
     * Vérifier la relation ordre entre 2 ensembles
     * en fonction de l'opération de jointure
     * @param setA : emsemble 1
     * @param setB : emsemble 1
     */
    public boolean checkRelationOrder(Set<T> setA, Set<T> setB){
        if (jointure){
            return relationOrder(setB,setA); //union
        }
        else {
            return relationOrder(setA,setB);
        }
    }

    /**
     *  Vérifier  si setA est inclus dans setB
     * @param setA emsemble 1
     * @param setB emsemble 2
     */
    public boolean relationOrder(Set<T> setA, Set<T> setB){
        if (Objects.isNull(setA) && Objects.isNull(setA)){
            return true;
        }
        else if(Objects.isNull(setA)){
            return false;
        }
        else if(Objects.isNull(setB)){
            return true;
        }
        else {
            return !(setA.containsAll(setB));
        }
    }

    /**
     * Réalise l'operation de jointure de 2 ensembles
     * @param setA : emsemble 1
     * @param setB : emsemble 1
     * @return : résultat
     */
    public Set<T> operationJointure(Set<T> setA, Set<T> setB){
        if(jointure){
            return  union(setA,setB);
        }
        else  {
            return intersection(setA,setB);
        }

    }

    /**
     * Union de setA et setB
     * @param setA ensemble 1
     * @param setB ensemble 1
     * @return Union de setA et setB
     */
    public Set<T> union(Set<T> setA, Set<T> setB){
        if (Objects.isNull(setA) && Objects.isNull(setA)){
            return null;
        }
        else if(Objects.isNull(setA)){
            return setB;
        }
        else if(Objects.isNull(setB)){
            return setA;
        }
        else {
            Set<T> inter = new HashSet<>(setA);
            inter.addAll(setB);
            return inter;
        }
    }

    /**
     * L'intersection entre setA et setB
     * @param setA ensemble 1
     * @param setB ensemble 2
     * @return intersection entre setA et setB
     */
    public Set<T> intersection(Set<T> setA, Set<T> setB){
        if (Objects.isNull(setA) && Objects.isNull(setA)){
            return null;
        }
        else if(Objects.isNull(setA)){
            return setB;
        }
        else if(Objects.isNull(setB)){
            return setA;
        }
        else {
            Set<T> inter = new HashSet<>(setA);
            inter.retainAll(setB);
            return inter;
        }
    }

    /**
     * La liste des arcs pour ce label
     *
     * [(label, ? ) , (label, ? ), ...... ]
     *
     * @param label: label
     * @return : liste des arcs pour ce label
     */
    public ArrayList<Pair<Integer, Integer>> getLabel(Integer label){
        ArrayList<Pair<Integer, Integer>> list = new ArrayList<>();
        for (Pair<Integer,Integer> l : F){
            if (l.getFst().equals(label)){
                list.add(l);
            }
        }
        // changer l'ordre de l'ajouts des labels lors de la recherche
        if (!isFlow)
            Collections.reverse(list);
        return list;
    }

    public Map<Integer, Set<T>> getAnalyse() {
        return analyse;
    }

    public void printProgram(Program program) {
        PrinterVisitor printerVisitor = new PrinterVisitor();
        List<String> a = program.accept(printerVisitor);
        System.out.println("Programme :\n");
        for (String s: a)
            System.out.println(s);
        System.out.println();

    }

    @Override
    public String toString() {
        String jointureString = "union";
        if (jointure)
            jointureString = "intersection";
        String flowString = "avant";
        if (! isFlow)
            flowString = "arriere";
        return "\nanalyse : " + analyseVisitor.getClass().getName().substring(10, 12) +
                "\nanalyse " + flowString +
                "\njointure : " + jointureString +
                "\nF : " + F +
                "\nE : " + E +
                "\ni : " + i + "\n\n";
    }
}
