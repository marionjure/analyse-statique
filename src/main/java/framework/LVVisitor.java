package framework;

import analyse.FVvisitor;
import ast.*;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Live Variable analysis
 */

public class LVVisitor extends DefaultVisitor<Set<Identifier>> implements AnalyseVisitor<Identifier> {

    /**
     * Visitor pour calculer FV d'un programme
     */
    private final FVvisitor fVVisitor;

    /**
     * L'analyse pour chaque label
     */
    private Map<Integer, Set<Identifier>> analyse;

    private Program program;


    public LVVisitor() {
        super(new HashSet<>());
        this.fVVisitor = new FVvisitor(new HashSet<>());
    }

    @Override
    public void setAnalyse(Map<Integer, Set<Identifier>> analyse) {
        this.analyse = analyse;
    }

    @Override
    public Set<Identifier> visit2(Call call) {
        // reprendre l'état de l'entrée du call
        return analyse.get(call.getLabelOut());
    }

    @Override
    public Set<Identifier> visit(Call call) {
        // vider lors de l'entrée dans la procédure
        return new HashSet<>();
    }

    @Override
    public Set<Identifier> visit(Node node) {
        return node.accept(this);
    }

    @Override
    public void setProgram(Program program) {
        this.program = program;
    }

    // kill gen

    @Override
    public Set<Identifier> visit(Skip skip) {
        return analyse.get(skip.getLabel());
    }

    @Override
    public Set<Identifier> visit(End end) {
        return analyse.get(end.getLabel());
    }

    @Override
    public Set<Identifier> visit(Begin begin) {
        return analyse.get(begin.getLabel());
    }

    @Override
    public Set<Identifier> visit(BExpParenthesis bExpParenthesis) {
        Set<Identifier> valeur = new HashSet<>(analyse.get(bExpParenthesis.getLabel()));
        valeur.addAll(bExpParenthesis.getBexpression().accept(this.fVVisitor));
        return valeur;
    }

    @Override
    public Set<Identifier> visit(BExpBinary bExpBinary) {
        Set<Identifier> valeur = new HashSet<>(analyse.get(bExpBinary.getLabel()));
        valeur.addAll(bExpBinary.getLeft().accept(this.fVVisitor));
        valeur.addAll(bExpBinary.getRight().accept(this.fVVisitor));
        return valeur;
    }

    @Override
    public Set<Identifier> visit(BExpNot bExpNot) {
        Set<Identifier> valeur = new HashSet<>(analyse.get(bExpNot.getLabel()));
        valeur.addAll(bExpNot.getExpression().accept(this.fVVisitor));
        return valeur;
    }

    @Override
    public Set<Identifier> visit(AExpBinary aExpBinary) {
        Set<Identifier> valeur = new HashSet<>(analyse.get(aExpBinary.getLabel()));
        valeur.addAll(aExpBinary.getAexpressionLeft().accept(this.fVVisitor));
        valeur.addAll(aExpBinary.getAexpressionRight().accept(this.fVVisitor));
        return valeur;
    }

    @Override
    public Set<Identifier> visit(Affectation affectation) {
        Set<Identifier> valeur = new HashSet<>(analyse.get(affectation.getLabel()));
        Set<Identifier> identifiers = fVVisitor.visit(affectation.getAexpression());
        // supprime la variable de l'affectation
        valeur.removeAll(fVVisitor.visit(affectation.getIdentifier()));
        // ajoute les variables de l'expression
        valeur.addAll(identifiers);
        return valeur;
    }

}
