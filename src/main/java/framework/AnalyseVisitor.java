package framework;

import ast.Call;
import ast.Node;
import ast.End;
import ast.Program;

import java.util.Map;
import java.util.Set;

public interface AnalyseVisitor<T> {
    public abstract void setAnalyse(Map<Integer, Set<T>> analyse);
    public abstract Set<T> visit2(Call call);
    public abstract Set<T> visit(Node node);
    public void setProgram(Program program);
}
