package framework;

import analyse.AExpVisitor;
import analyse.FVvisitor;
import ast.*;
import support.Pair;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Classe pour l'analyse Very Busy Expression
 */
public class VBVisitor extends DefaultVisitor<Set<Expression>> implements AnalyseVisitor<Expression> {
    /**
     * Visitor pour calculer FV d'un programme
     */
    private FVvisitor fVvisitor;

    /**
     *  Visitor pour calculer aExp* d'un programme
     */
    private AExpVisitor aExpVisitor;

    /**
     * L'ensemble aExp*
     */
    private Set<Aexpression> aExp;

    /**
     * Map qui associe une variable à l'ensemble de ces expressions
     *
     * {a : {a+1,b+a}}
     */
    private Map<Identifier,Set<Aexpression>> dico;

    /**
     * L'analyse pour chaque label
     */
    private Map<Integer,Set<Expression>> analyse;

    private Map<Integer, Set<Pair<Identifier,Aexpression>>> declaration;
    /**
     * le programme
     */
    private Program program;

    public VBVisitor() {
        super(new HashSet<>());
        this.aExpVisitor = new AExpVisitor(new HashSet<>());
        this.fVvisitor = new FVvisitor(new HashSet<>());
        this.dico = new HashMap<>();
    }

    @Override
    public void setAnalyse(Map<Integer, Set<Expression>> analyse) {
        this.analyse = analyse;
    }

    /**
     * Remplir le dico pour associer les expressions à chaque variable
     */
    public void remplir(){
        for (Aexpression aexpression : aExp){
            Set<Identifier> identifiers = aexpression.accept(fVvisitor);
            for (Identifier identifier : identifiers){
                if (!dico.containsKey(identifier)){
                    dico.put(identifier,new HashSet<>());
                }
                dico.get(identifier).add(aexpression);
            }
        }
    }

    @Override
    public void setProgram(Program program) {
        this.program = program;
        this.aExp = this.aExpVisitor.visit(program);
        MFP<Pair<Identifier, Aexpression>> mfp = new MFP(program,false,false,new DDVisitor(),new HashSet<>(),false);
        declaration = mfp.getAnalyse();
        remplir();
    }

    @Override
    public Set<Expression> visit2(Call call) {
        Set<Expression> set;
        if (analyse.get(call.getLabelOut()) != null) {
            set = new HashSet<>(analyse.get(call.getLabelOut()));
        } else {
            set = new HashSet<>();
        }
        for(Declaration d : program.getDeclarations()){
            if (d.getName().equals(call.getIdentifier())){
                if (d.getNameRes().isPresent()){
                    for (var e : declaration.get(call.getLabelOut())){
                        if (d.getNameRes().get().equals(e.getFst())){
                            set.add(e.getSnd());
                        }
                    }
                }
            }
        }
        return set;
    }

    @Override
    public Set<Expression> visit(Node node) {
        return node.accept(this);
    }

    @Override
    public Set<Expression> visit(Skip skip) {
        return analyse.get(skip.getLabel());
    }

    @Override
    public Set<Expression> visit(Begin begin) {
        return new HashSet<>(analyse.get(begin.getLabel()));
    }


    @Override
    public Set<Expression> visit(BExpParenthesis bExpParenthesis) {
        Set<Expression> valeur = new HashSet<>(analyse.get(bExpParenthesis.getLabel()));
        valeur.addAll(bExpParenthesis.getBexpression().accept(this.aExpVisitor));
        return valeur;
    }

    @Override
    public Set<Expression> visit(BExpBinary bExpBinary) {
        Set<Expression> valeur = new HashSet<>(analyse.get(bExpBinary.getLabel()));
        valeur.addAll(bExpBinary.accept(this.aExpVisitor));
        return valeur;
    }

    @Override
    public Set<Expression> visit(BExpNot bExpNot) {
        Set<Expression> valeur = new HashSet<>(analyse.get(bExpNot.getLabel()));
        valeur.addAll(bExpNot.getExpression().accept(this.aExpVisitor));
        return valeur;
    }

    @Override
    public Set<Expression> visit(Affectation affectation) {
        Set<Expression> valeur = new HashSet<>(analyse.get(affectation.getLabel()));
        Set<Identifier> identifiers = fVvisitor.visit(affectation.getAexpression());
        if (identifiers.size()!=0) {
            if (identifiers.contains(affectation.getIdentifier())) {
                valeur.removeAll(dico.get(affectation.getIdentifier()));
            }
            if (affectation.getAexpression().getClass().getName().compareTo("ast.AExpIdent")!=0) {
                valeur.add(affectation.getAexpression());
            }
        }
        return valeur;
    }
}
