package ast;

public class BExpBinary extends Bexpression{
    private final Aexpression left;
    private final EnumBinaryOp operator;
    private final Aexpression right;

    public BExpBinary(Position position, Aexpression left, EnumBinaryOp operator, Aexpression right) {
        this.position = position;
        this.left = left;
        this.operator = operator;
        this.right = right;
    }

    public Aexpression getLeft() {
        return left;
    }

    public EnumBinaryOp getOperator() {
        return operator;
    }

    public Aexpression getRight() {
        return right;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return left.toString() + operator + right ;
    }
}
