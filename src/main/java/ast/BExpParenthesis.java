package ast;

public class BExpParenthesis extends Bexpression{
    private final Bexpression bexpression;

    public BExpParenthesis(Position position, Bexpression bexpression) {
        this.position = position;
        this.bexpression = bexpression;
    }

    public Bexpression getBexpression() {
        return bexpression;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return  bexpression.toString() ;
    }
}
