package ast;

import java.util.Objects;

public class AExpParenthesis extends Aexpression{
    private final Aexpression aexpression;

    public AExpParenthesis(Position position, Aexpression aexpression) {
        this.position = position;
        this.aexpression = aexpression;
    }

    public Aexpression getAexpression() {
        return aexpression;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AExpParenthesis that = (AExpParenthesis) o;
        return Objects.equals(aexpression, that.aexpression);
    }

    @Override
    public String toString() {
        return  aexpression +"";
    }

    @Override
    public int hashCode() {
        return Objects.hash(aexpression);
    }
}
