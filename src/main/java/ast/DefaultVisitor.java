package ast;

public class DefaultVisitor<T> implements Visitor<T>{
    protected T defaultValue;

    public DefaultVisitor(T defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public T visit(Program program) {
        if (program.getName().isPresent())
            program.getName().get().accept(this);
        for (Declaration declaration : program.getDeclarations())
            declaration.accept(this);
        return program.getStatements().accept(this);
    }

    @Override
    public T visit(AExpIdent aExpIdent) {
        return aExpIdent.getIdentifier().accept(this);
    }

    @Override
    public T visit(AExpConst aExpConst) {
        return aExpConst.getConstant().accept(this);
    }

    @Override
    public T visit(AExpBinary aExpBinary) {
        aExpBinary.getAexpressionLeft().accept(this);
        return aExpBinary.getAexpressionRight().accept(this);
    }

    @Override
    public T visit(AExpOpposite aExpOpposite) {
        return aExpOpposite.getAexpression().accept(this);
    }

    @Override
    public T visit(AExpParenthesis aExpParenthesis) {
        return aExpParenthesis.getAexpression().accept(this);
    }

    @Override
    public T visit(BExpTrue bExpTrue) {
        return defaultValue;
    }

    @Override
    public T visit(BExpFalse bExpFalse) {
        return defaultValue;
    }

    @Override
    public T visit(BExpBinary bExpBinary) {
        bExpBinary.getLeft().accept(this);
        return bExpBinary.getRight().accept(this);
    }

    @Override
    public T visit(BExpNot bExpNot) {
        return bExpNot.getExpression().accept(this);
    }

    @Override
    public T visit(BExpParenthesis bExpParenthesis) {
        return bExpParenthesis.getBexpression().accept(this);
    }

    @Override
    public T visit(BlockOnly blockOnly) {
        return blockOnly.getStatement().accept(this);
    }

    @Override
    public T visit(BlockParenthesis blockParenthesis) {
        return blockParenthesis.getStatements().accept(this);
    }

    @Override
    public T visit(Declaration declaration) {
        declaration.getName().accept(this);
        declaration.getlDeclIdent().accept(this);
        if (declaration.getTypeRes().isPresent())
            declaration.getTypeRes().get().accept(this);
        if (declaration.getNameRes().isPresent())
            declaration.getNameRes().get().accept(this);
        return declaration.getStatements().accept(this);
    }

    @Override
    public T visit(LDeclIdent lDeclIdent) {
        T curr = defaultValue;
        for (Type type : lDeclIdent.getTypes())
            curr = type.accept(this);
        for (Identifier parameter : lDeclIdent.getParameters())
            curr = parameter.accept(this);
        return curr;
    }

    @Override
    public T visit(LDeclVariables lDeclVariables) {
        T curr = lDeclVariables.getDeclVariables().accept(this);
        for (LDeclVariables lDeclVariable : lDeclVariables.getlDeclVariables()) {
            curr = lDeclVariable.accept(this);
        }
        return curr;
    }

    @Override
    public T visit(DeclVariables declVariables) {
        declVariables.getType().accept(this);
        return declVariables.getlIdentifier().accept(this);
    }

    @Override
    public T visit(LIdentifier lIdentifier) {
        T curr = defaultValue;
        for (Identifier identifier : lIdentifier.getIdentifiers()) {
            curr = identifier.accept(this);
        }
        return curr;
    }

    @Override
    public T visit(Statements statements) {
        T curr = statements.getStatement().accept(this);
        for (Statements statement : statements.getStatements())
            curr = statement.accept(this);
        return curr;
    }

    @Override
    public T visit(Statement statement) {
        return statement.accept(this);
    }

    @Override
    public T visit(LAexpression lAexpression) {
        T curr = defaultValue;
        for (Aexpression aexpression : lAexpression.getAexpressionList())
            curr = aexpression.accept(this);
        return curr;
    }

    @Override
    public T visit(Identifier identifier) {
        return defaultValue;
    }

    @Override
    public T visit(Constant constant) {
        return defaultValue;
    }

    @Override
    public T visit(End end) {
        return defaultValue;
    }

    @Override
    public T visit(Begin begin) {
        return defaultValue;
    }

    @Override
    public T visit(Skip skip) {
        return defaultValue;
    }

    @Override
    public T visit(Affectation affectation) {
        affectation.getIdentifier().accept(this);
        return affectation.getAexpression().accept(this);
    }

    @Override
    public T visit(If sif) {
        sif.getBexpression().accept(this);
        T curr = sif.getIfBlock().accept(this);
        if (sif.getElseBlock().isPresent())
            curr = sif.getElseBlock().get().accept(this);
        return curr;
    }

    @Override
    public T visit(While swhile) {
        swhile.getBexpression().accept(this);
        return swhile.getBlock().accept(this);
    }

    @Override
    public T visit(Call call) {
        call.getIdentifier().accept(this);
        return call.getlAexpression().accept(this);
    }

    @Override
    public T visit(Int tint) {
        return defaultValue;
    }

    @Override
    public T visit(Boolean tboolean) {
        return defaultValue;
    }
}
