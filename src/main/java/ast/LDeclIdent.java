package ast;

import java.util.List;

public class LDeclIdent extends Node{
    private List<Type> types;
    private List<Identifier> parameters;

    public LDeclIdent(Position position,List<Type> types, List<Identifier> parameters) {
        this.position=position;
        this.types = types;
        this.parameters = parameters;
    }

    public List<Type> getTypes() {
        return types;
    }

    public List<Identifier> getParameters() {
        return parameters;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
