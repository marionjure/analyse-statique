package ast;

public class Affectation extends Statement{

    private Identifier identifier;
    private Aexpression aexpression;

    public Affectation(Position position, Identifier identifier, Aexpression aexpression) {
        this.position = position;
        this.identifier = identifier;
        this.aexpression = aexpression;
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public Aexpression getAexpression() {
        return aexpression;
    }

    @Override
    public String toString() {
        return  identifier + ":=" + aexpression;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
