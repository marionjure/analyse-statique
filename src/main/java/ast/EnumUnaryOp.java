package ast;

public enum EnumUnaryOp {
    NOT {
        public String toString() {
            return "not";
        }
    },
    MINUS {
        public String toString() {
            return "-";
        }
    }
}