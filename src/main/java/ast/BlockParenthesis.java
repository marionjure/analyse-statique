package ast;

public class BlockParenthesis extends Block {
    private Statements statements;

    public BlockParenthesis(Position position,Statements statements) {
        this.position = position;
        this.statements = statements;
    }

    public Statements getStatements() {
        return statements;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
