package ast;

public class Boolean extends Type{

    public Boolean(Position position) {
        this.position = position;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
