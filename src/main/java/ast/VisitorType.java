package ast;

public interface VisitorType<T> {
    T visit(Int tint);
    T visit(Boolean tboolean);
}
