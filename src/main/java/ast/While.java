package ast;

public class While extends Statement{

    private Bexpression bexpression;
    private Block block;

    public While(Position position, Bexpression bexpression, Block block) {
        this.position = position;
        this.bexpression = bexpression;
        this.block = block;
    }

    public Bexpression getBexpression() {
        return bexpression;
    }

    public Block getBlock() {
        return block;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
