package ast;

import java.util.Objects;

public class AExpBinary extends Aexpression{
    private Aexpression aexpressionLeft;
    private EnumBinaryOp binaryOp;
    private Aexpression aexpressionRight;

    public AExpBinary(Position position, Aexpression aexpressionLeft, EnumBinaryOp binaryOp, Aexpression aexpressionRight) {
        this.position = position;
        this.aexpressionLeft = aexpressionLeft;
        this.binaryOp = binaryOp;
        this.aexpressionRight = aexpressionRight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AExpBinary that = (AExpBinary) o;
        return Objects.equals(aexpressionLeft, that.aexpressionLeft) && binaryOp == that.binaryOp && Objects.equals(aexpressionRight, that.aexpressionRight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(aexpressionLeft, binaryOp, aexpressionRight);
    }

    public Aexpression getAexpressionLeft() {
        return aexpressionLeft;
    }

    public EnumBinaryOp getBinaryOp() {
        return binaryOp;
    }

    @Override
    public String toString() {
        return  aexpressionLeft.toString() + binaryOp + aexpressionRight.toString() ;
    }

    public Aexpression getAexpressionRight() {
        return aexpressionRight;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
