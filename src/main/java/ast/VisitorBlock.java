package ast;

public interface VisitorBlock<T> {
    T visit(BlockOnly blockOnly);
    T visit(BlockParenthesis blockParenthesis);
}
