package ast;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Program extends Node{
    private Optional<Identifier> name;
    private List<Declaration> declarations;
    private LDeclVariables lDeclVariables;
    private Statements statements;

    public Program(Position position,Optional<Identifier> name, List<Declaration> declarations, LDeclVariables lDeclVariables, Statements statements) {
        this.position = position;
        this.name = name;
        this.declarations = declarations;
        this.lDeclVariables = lDeclVariables;
        this.statements = statements;
    }

    public Optional<Identifier> getName() {
        return name;
    }

    public List<Declaration> getDeclarations() {
        return declarations;
    }

    public LDeclVariables getlDeclVariables() {
        return lDeclVariables;
    }

    public Statements getStatements() {
        return statements;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
