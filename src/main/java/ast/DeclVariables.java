package ast;

public class DeclVariables extends Node{
    private Type type;
    private LIdentifier lIdentifier;

    public DeclVariables(Position position,Type type, LIdentifier lIdentifier) {
        this.position= position;
        this.type = type;
        this.lIdentifier = lIdentifier;
    }

    public Type getType() {
        return type;
    }

    public LIdentifier getlIdentifier() {
        return lIdentifier;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
