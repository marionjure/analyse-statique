package ast;

public interface VisitorGlo<T> {
    T visit(Declaration declaration);
    T visit(LDeclIdent lDeclIdent);
    T visit(LDeclVariables lDeclVariables);
    T visit(DeclVariables declVariables);
    T visit(LIdentifier lIdentifier);
    T visit(Statements statements);
    T visit(Statement statement);
    T visit(LAexpression lAexpression);
    T visit(Identifier identifier);
    T visit(Constant constant);
    T visit(End end);
    T visit(Begin begin);
}
