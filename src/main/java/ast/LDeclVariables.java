package ast;

import java.util.List;

public class LDeclVariables extends Node{
    private DeclVariables declVariables;
    private List<LDeclVariables> lDeclVariables;

    public LDeclVariables(Position position,DeclVariables declVariables, List<LDeclVariables> lDeclVariables) {
        this.position= position;
        this.declVariables = declVariables;
        this.lDeclVariables = lDeclVariables;
    }

    public DeclVariables getDeclVariables() {
        return declVariables;
    }

    public List<LDeclVariables> getlDeclVariables() {
        return lDeclVariables;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
