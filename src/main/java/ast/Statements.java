package ast;

import java.util.List;

// todo à change block
public class Statements extends Node {
    private Statement statement;
    private List<Statements> statements;

    public Statements(Position position,Statement statement, List<Statements> statements) {
        this.position = position;
        this.statement = statement;
        this.statements = statements;
    }

    public Statement getStatement() {
        return statement;
    }

    public List<Statements> getStatements() {
        return statements;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
