package ast;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import parser.WhileBaseVisitor;
import parser.WhileLexer;
import parser.WhileParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class Build extends WhileBaseVisitor<Node> {
    // Building an AST Position from an ANTLR Context
    private static Position position(ParserRuleContext ctx) {
        return new Position(ctx.start.getLine(),
                ctx.start.getCharPositionInLine());
    }

    @SuppressWarnings("unchecked")
    private <T> List<T> makeList(List<? extends ParserRuleContext> contexts) {
        List<T> nodes = new ArrayList<>();
        for (ParserRuleContext context : contexts)
            nodes.add((T) context.accept(this));
        return nodes;
    }

    private EnumBinaryOp getBinaryOp(Token token){
        switch(token.getType()){
            case WhileLexer.Plus: return EnumBinaryOp.ADD;
            case WhileLexer.Minus: return EnumBinaryOp.SUB;
            case WhileLexer.Multiplication: return EnumBinaryOp.MUL;
            case WhileLexer.Division: return EnumBinaryOp.DIV;
            case WhileLexer.Different: return EnumBinaryOp.DIFF;
            case WhileLexer.LesserThan: return EnumBinaryOp.LT;
            case WhileLexer.LesserOrEqual: return EnumBinaryOp.LTE;
            case WhileLexer.GreaterThan: return EnumBinaryOp.GT;
            case WhileLexer.GreaterOrEqual: return EnumBinaryOp.GTE;
        }
        return EnumBinaryOp.EQ;
    }

    private EnumUnaryOp getUnaryOp(Token token){
        switch(token.getType()){
            case WhileLexer.Minus: return EnumUnaryOp.MINUS;
            case WhileLexer.Negation: return EnumUnaryOp.NOT;
        }
        throw new Error("ast.Build: Undefined unary operation");
    }

    @Override public Node visitProgram(WhileParser.ProgramContext ctx) {
        Optional<Identifier> identifier =
                ctx.identifier() == null ? Optional.empty() :
                        Optional.of((Identifier) ctx.identifier().accept(this));
        List<Declaration> declarations = makeList(ctx.declaration());
        LDeclVariables lDeclVariables= (LDeclVariables) ctx.lDeclVariables().accept(this);
        Statements statements = (Statements) ctx.statements().accept(this);
        return new Program(position(ctx),identifier, declarations, lDeclVariables, statements);
    }
    @Override public Declaration visitDeclaration(WhileParser.DeclarationContext ctx) {
        Identifier name = (Identifier) ctx.identifier().get(0).accept(this);
        LDeclIdent lDeclIdent = (LDeclIdent) ctx.lDeclIdent().accept(this);
        Type type = (Type) ctx.type().accept(this);
        Optional<Identifier> nameRes = Optional.empty();
        if(ctx.identifier().size()==2) {
            nameRes = Optional.of((Identifier) ctx.identifier().get(1).accept(this));
        }
        Statements  statements = (Statements ) ctx.statements().accept(this);
        return new Declaration(position(ctx),name,lDeclIdent, Optional.of(type), nameRes,statements);
    }

    @Override public LDeclIdent visitLDeclIdent(WhileParser.LDeclIdentContext ctx) {
        List<Type> types = makeList(ctx.type());
        List<Identifier> names = makeList(ctx.identifier());
        return new LDeclIdent(position(ctx),types,names);
    }
    @Override
    public Node visitLDeclVariables(WhileParser.LDeclVariablesContext ctx) {
        DeclVariables declVariables=(DeclVariables)  ctx.declVariables().accept(this);
        List<LDeclVariables> lDeclVariables = makeList(ctx.lDeclVariables());
        return new LDeclVariables(position(ctx),declVariables,lDeclVariables);
    }

    @Override
    public Node visitDeclVariables(WhileParser.DeclVariablesContext ctx) {
        Type type =(Type) ctx.type().accept(this);
        LIdentifier lIdentifier = (LIdentifier) ctx.lIdentifier().accept(this);
        return new DeclVariables(position(ctx),type,lIdentifier);
    }

    @Override
    public Node visitLIdentifier(WhileParser.LIdentifierContext ctx) {
        List<Identifier> identifier= makeList(ctx.identifier());
        return new LIdentifier(position(ctx),identifier);
    }

    @Override
    public Node visitBlockOnly(WhileParser.BlockOnlyContext ctx) {
        Statement statement = (Statement) ctx.statement().accept(this);
        return new BlockOnly(position(ctx),statement);
    }

    @Override
    public Node visitBlockParenthesis(WhileParser.BlockParenthesisContext ctx) {
        Statements statements =(Statements) ctx.statements().accept(this);
        return new BlockParenthesis(position(ctx),statements);
    }

    @Override
    public Node visitStatements(WhileParser.StatementsContext ctx) {
        Statement statement = (Statement) ctx.statement().accept(this);
        List<Statements> statements = makeList(ctx.statements());
        return new Statements(position(ctx),statement,statements);
    }

    @Override
    public Node visitSkip(WhileParser.SkipContext ctx) {
        return new Skip(position(ctx));
    }

    @Override
    public Node visitAffectation(WhileParser.AffectationContext ctx) {
        Identifier identifier = (Identifier) ctx.identifier().accept(this);
        Aexpression aexpression = (Aexpression) ctx.aexpression().accept(this);
        return new Affectation(position(ctx), identifier, aexpression);
    }

    @Override
    public Node visitIf(WhileParser.IfContext ctx) {
        Bexpression bexpression = (Bexpression) ctx.bexpression().accept(this);
        Block blockIf = (Block) ctx.block(0).accept(this);
        Optional<Block> blockElse = Optional.empty();
        if (ctx.block().size() == 2)
            blockElse = Optional.of((Block) ctx.block(1).accept(this));
        return new If(position(ctx), bexpression, blockIf, blockElse);
    }

    @Override
    public Node visitWhile(WhileParser.WhileContext ctx) {
        Bexpression bexpression = (Bexpression) ctx.bexpression().accept(this);
        Block block = (Block) ctx.block().accept(this);
        return new While(position(ctx), bexpression, block);
    }

    @Override
    public Node visitCall(WhileParser.CallContext ctx) {
        Identifier identifier = (Identifier) ctx.identifier().accept(this);
        LAexpression lAexpression = (LAexpression) ctx.lAexpression().accept(this);
        return new Call(position(ctx), identifier, lAexpression);
    }

    @Override
    public Node visitLAexpression(WhileParser.LAexpressionContext ctx) {
        List<Aexpression> aexpressionList = new ArrayList<>();
        for (WhileParser.AexpressionContext aexpression: ctx.aexpression())
            aexpressionList.add((Aexpression) aexpression.accept(this));
        return new LAexpression(position(ctx), aexpressionList);
    }

    @Override
    public Node visitAExpBinary(WhileParser.AExpBinaryContext ctx) {
        Aexpression aexpressionLeft = (Aexpression) ctx.aexpression(0).accept(this);
        EnumBinaryOp op = getBinaryOp(ctx.opa);
        Aexpression aexpressionRight = (Aexpression) ctx.aexpression(1).accept(this);

        return new AExpBinary(position(ctx), aexpressionLeft, op, aexpressionRight);
    }

    @Override
    public Node visitAExpConst(WhileParser.AExpConstContext ctx) {
        return new AExpConst(position(ctx), (Constant) ctx.constant().accept(this));
    }

    @Override
    public Node visitAExpOpposite(WhileParser.AExpOppositeContext ctx) {
        Aexpression aexpression = (Aexpression) ctx.aexpression().accept(this);
        return new AExpOpposite(position(ctx), aexpression);
    }

    @Override
    public Node visitAExpIdent(WhileParser.AExpIdentContext ctx) {
        return new AExpIdent(position(ctx), (Identifier) ctx.identifier().accept(this) );
    }

    @Override
    public Node visitAExpParenthesis(WhileParser.AExpParenthesisContext ctx) {
        Aexpression aexpression = (Aexpression) ctx.aexpression().accept(this);
        return new AExpParenthesis(position(ctx), aexpression);
    }

    @Override
    public Node visitBExpTrue(WhileParser.BExpTrueContext ctx) {
        return new BExpTrue(position(ctx));
    }

    @Override
    public Node visitBExpFalse(WhileParser.BExpFalseContext ctx) {
        return new BExpFalse(position(ctx));
    }

    @Override
    public Node visitBExpBinary(WhileParser.BExpBinaryContext ctx) {
        Aexpression left = (Aexpression) ctx.aexpression(0).accept(this);
        Aexpression right = (Aexpression) ctx.aexpression(1).accept(this);
        return new BExpBinary(position(ctx), left, getBinaryOp(ctx.opr), right);
    }

    @Override
    public Node visitBExpNot(WhileParser.BExpNotContext ctx) {
        Bexpression bexpression = (Bexpression) ctx.bexpression().accept(this);
        return new BExpNot(position(ctx), EnumUnaryOp.NOT, bexpression);
    }

    @Override
    public Node visitBExpParenthesis(WhileParser.BExpParenthesisContext ctx) {
        Bexpression bexpression = (Bexpression) ctx.bexpression().accept(this);
        return new BExpParenthesis(position(ctx), bexpression);
    }

    @Override
    public Node visitInt(WhileParser.IntContext ctx) {
        return new Int(position(ctx));
    }

    @Override
    public Node visitBoolean(WhileParser.BooleanContext ctx) {
        return new Boolean(position(ctx));
    }

    @Override
    public Node visitIdentifier(WhileParser.IdentifierContext ctx) {
        String name = ctx.getText();
        return new Identifier(position(ctx),name);
    }

    @Override
    public Node visitConstant(WhileParser.ConstantContext ctx) {
        int value = Integer.parseInt(ctx.getText());
        return new Constant(position(ctx),value);
    }






}
