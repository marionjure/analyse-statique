package ast;

import java.util.Objects;

//todo à supprimer
public class AExpConst extends Aexpression{
    private Constant constant;

    public AExpConst(Position position, Constant constant) {
        this.position = position;
        this.constant = constant;
    }

    public AExpConst(int constant) {
        this.position = null;
        this.constant = new Constant(null,constant);
    }

    public Constant getConstant() {
        return constant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AExpConst aExpConst = (AExpConst) o;
        return Objects.equals(constant.getValue(), aExpConst.constant.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(constant.getValue());
    }

    @Override
    public String toString() {
        return constant.getValue()+"";
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
