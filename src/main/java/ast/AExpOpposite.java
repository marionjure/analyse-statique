package ast;

import java.util.Objects;

public class AExpOpposite extends Aexpression {
    private final Aexpression aexpression;

    public AExpOpposite(Position position, Aexpression aexpression) {
        this.position = position;
        this.aexpression = aexpression;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AExpOpposite that = (AExpOpposite) o;
        return Objects.equals(aexpression, that.aexpression);
    }

    @Override
    public int hashCode() {
        return Objects.hash(aexpression);
    }

    public Aexpression getAexpression() {
        return aexpression;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
