package ast;

//todo à supprimer
public class BlockOnly extends Block{
    private Statement statement;

    public BlockOnly(Position position,Statement statement) {
        this.position= position;
        this.statement = statement;
    }

    public Statement getStatement() {
        return statement;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
