package ast;

public class Call extends Statement{
    private Identifier identifier;
    private LAexpression lAexpression;
    private int labelOut;
    private Integer labelIn;

    public Call(Position position, Identifier identifier, LAexpression lAexpression) {
        this.position = position;
        this.identifier = identifier;
        this.lAexpression = lAexpression;
    }

    public Integer getLabelIn() {
        return labelIn;
    }

    public void setLabelIn(Integer labelIn) {
        this.labelIn = labelIn;
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public LAexpression getlAexpression() {
        return lAexpression;
    }

    public int getLabelOut() {
        return labelOut;
    }

    public void setLabelOut(int labelOut) {
        this.labelOut = labelOut;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
