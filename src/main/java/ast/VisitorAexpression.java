package ast;

public interface VisitorAexpression<T> {
    T visit(AExpIdent aExpIdent);
    T visit(AExpConst aExpConst);
    T visit(AExpBinary aExpBinary);
    T visit(AExpOpposite aExpOpposite);
    T visit(AExpParenthesis aExpParenthesis);
}
