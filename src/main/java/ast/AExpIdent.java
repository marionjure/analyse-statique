package ast;

import java.util.Objects;

//Todo à supprimer
public class AExpIdent extends Aexpression{
    private Identifier identifier;

    public AExpIdent(Position position, Identifier identifier) {
        this.position = position;
        this.identifier = identifier;
    }
    public AExpIdent(Identifier identifier) {
        this.position = null;
        this.identifier = identifier;
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AExpIdent aExpIdent = (AExpIdent) o;
        return Objects.equals(identifier.getName(), aExpIdent.identifier.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier.getName());
    }

    @Override
    public String toString() {
        return identifier.getName();
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
