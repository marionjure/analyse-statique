package ast;

public class BExpTrue extends Bexpression{

    public BExpTrue(Position position) {
        this.position = position;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
