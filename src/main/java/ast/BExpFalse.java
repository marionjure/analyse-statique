package ast;

public class BExpFalse extends Node{

    public BExpFalse(Position position) {
        this.position = position;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
