package ast;

import java.util.Optional;

public class If extends Statement{
    private Bexpression bexpression;
    private Block ifBlock;
    private Optional<Block> elseBlock;

    public If(Position position, Bexpression bexpression, Block ifBlock, Optional<Block> elseBlock) {
        this.position = position;
        this.bexpression = bexpression;
        this.ifBlock = ifBlock;
        this.elseBlock = elseBlock;
    }

    public Bexpression getBexpression() {
        return bexpression;
    }

    public Block getIfBlock() {
        return ifBlock;
    }

    public Optional<Block> getElseBlock() {
        return elseBlock;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
