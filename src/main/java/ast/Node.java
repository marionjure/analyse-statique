package ast;

public abstract class Node {
    protected Position position;
    private int label;

    public Position getPosition() {
        return position;
    }

    public int getLabel() {
        return label;
    }

    public void setLabel(int label) {
        this.label = label;
    }

    // todo coder
    public abstract <T> T accept(Visitor<T> visitor);
}
