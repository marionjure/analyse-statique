package ast;

import java.util.Optional;

public class Declaration extends Node{
    private Identifier name;
    private LDeclIdent lDeclIdent;
    private Optional<Type> typeRes;
    private Optional<Identifier> nameRes;
    private Statements statements;
    private End end;
    private Begin begin;

    public Declaration(Position position,Identifier name, LDeclIdent lDeclIdent, Optional<Type> typeRes, Optional<Identifier> nameRes, Statements statements) {
        this.position= position;
        this.name = name;
        this.lDeclIdent = lDeclIdent;
        this.typeRes = typeRes;
        this.nameRes = nameRes;
        this.statements = statements;
        this.end = new End();
        this.begin = new Begin();
    }

    public Identifier getName() {
        return name;
    }

    public LDeclIdent getlDeclIdent() {
        return lDeclIdent;
    }

    public Optional<Type> getTypeRes() {
        return typeRes;
    }

    public Optional<Identifier> getNameRes() {
        return nameRes;
    }

    public Statements getStatements() {
        return statements;
    }

    public int getLabelIn() {
        return begin.getLabel();
    }

    public void setLabelIn(int labelIn) {
        begin.setLabel(labelIn);
    }

    public int getLabelOut() {
        return end.getLabel();
    }

    public void setLabelOut(int labelOut) {
        end.setLabel(labelOut);
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }

    public End getEnd() {
        return end;
    }

    public Begin getBegin() {
        return begin;
    }
}
