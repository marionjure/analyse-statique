package ast;

import java.util.List;

public class LIdentifier extends Node {
    private List<Identifier> identifiers;

    public LIdentifier(Position position,List<Identifier> identifiers) {
        this.position = position;
        this.identifiers = identifiers;
    }

    public List<Identifier> getIdentifiers() {
        return identifiers;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
