package ast;

public class Int extends Type{

    public Int(Position position) {
        this.position = position;
    }



    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
