package ast;

public class BExpNot extends Bexpression{
    private final EnumUnaryOp operation;
    private final Bexpression expression;

    public BExpNot(Position position, EnumUnaryOp operation, Bexpression expression) {
        this.position = position;
        this.operation = operation;
        this.expression = expression;
    }

    public EnumUnaryOp getOperation() {
        return operation;
    }

    public Bexpression getExpression() {
        return expression;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
