package ast;

// todo à supprimer
public class Skip extends Statement{

    public Skip(Position position) {
        this.position = position;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
