package ast;

public interface Visitor<T> extends
        VisitorAexpression<T>,
        VisitorBexpression<T>,
        VisitorBlock<T>,
        VisitorGlo<T>,
        VisitorStatement<T>,
        VisitorType<T>{
    T visit(Program program);

}
