package ast;

public class Constant extends Node{
    private int value;

    public Constant(Position position,Integer value) {
        this.position = position;
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
