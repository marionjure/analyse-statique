package ast;

import java.util.List;

public class LAexpression extends Node{

    private List<Aexpression> aexpressionList;

    public LAexpression(Position position, List<Aexpression> aexpressions) {
        this.position = position;
        this.aexpressionList = aexpressions;
    }

    public List<Aexpression> getAexpressionList() {
        return aexpressionList;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visit(this);
    }
}
