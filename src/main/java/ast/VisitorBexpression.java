package ast;

public interface VisitorBexpression<T> {
    T visit(BExpTrue bExpTrue);
    T visit(BExpFalse bExpFalse);
    T visit(BExpBinary bExpBinary);
    T visit(BExpNot bExpNot);
    T visit(BExpParenthesis bExpParenthesis);

}
