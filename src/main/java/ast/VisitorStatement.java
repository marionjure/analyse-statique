package ast;

public interface VisitorStatement<T> {
    T visit(Skip skip);
    T visit(Affectation affectation);
    T visit(If sif);
    T visit(While swhile);
    T visit(Call call);

}
