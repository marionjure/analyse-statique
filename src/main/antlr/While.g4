grammar While;

program:
   Program
   identifier?
   declaration*
   Begin  lDeclVariables statements End
   EOF;

declaration:
    Proc
    identifier
    OpenedParenthesis  lDeclIdent (Comma Res type identifier) ? ClosedParenthesis
    Begin statements End;

lDeclIdent: type identifier (Comma type identifier)*;

lDeclVariables: declVariables lDeclVariables* ;

declVariables: type lIdentifier Semicolon;

lIdentifier :identifier (Comma identifier)*;

block :
        statement                                       #BlockOnly
    |   OpenedParenthesis statements ClosedParenthesis  #BlockParenthesis
    ;

statements: statement ( Semicolon statements)*;

statement:
        Skip                                                                #Skip
    |   identifier ':=' aexpression                                         #Affectation
    |   If  bexpression Then  block ( Else block)?                          #If
    |   While  bexpression Do block                                         #While
    |   Call identifier OpenedParenthesis lAexpression ClosedParenthesis    #Call
    ;

lAexpression: aexpression (Comma aexpression)*;

aexpression:
        identifier                                                                                  #AExpIdent
    |   constant                                                                                    #AExpConst
    |   aexpression opa=(Plus | Minus|Multiplication |Division) aexpression                         #AExpBinary
    |   Minus aexpression                                                                           #AExpOpposite
    |   OpenedParenthesis aexpression ClosedParenthesis                                             #AExpParenthesis
    ;

bexpression:
        True                                                                                                                            #BExpTrue
    |   False                                                                                                                           #BExpFalse
    |   aexpression opr=(GreaterThan | GreaterOrEqual|LesserThan |LesserOrEqual|Different |Equal) aexpression                           #BExpBinary
    |   Negation  bexpression                                                                                                           #BExpNot
    | OpenedParenthesis bexpression ClosedParenthesis                                                                                   #BExpParenthesis
    ;

type:
        Int         #Int
    |   Boolean     #Boolean
    ;

identifier: (Letter)(Underscore|Letter|Digit)*;

constant: Digit+;

OpenedParenthesis: '(';
ClosedParenthesis: ')';
Semicolon: ';';
Comma: ',';
Underscore : '_';
True: 'true';
False: 'false';
Program: 'program';
Proc: 'proc';
Res: 'res';
Begin: 'begin';
End: 'end';
Skip: 'skip';
If : 'if';
While : 'while';
Then: 'then';
Else: 'else';
Do: 'do';
Call: 'call';
Minus: '-';
Plus: '+';
Multiplication: '*';
Division: '/';
GreaterThan: '>';
GreaterOrEqual: '>=';
Equal: '==';
LesserThan: '<';
LesserOrEqual: '<=';
Different: '<>';
Negation: 'not';
Int: 'int';
Boolean:'boolean';
Letter: [a-z];
Digit:  [0-9];

WS: [ \t\r\n]+ -> skip;