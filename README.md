# Analyse statique

Réalisation d’un analyseur pour le langage WHILE avec le framework ANTLR.

## Lancer le projet

Les arguments sont le nom du chemin du programme et le type d'analyse (ae : Analyse de disponibilité des expressions, rd : Analyse de validité des expression, vb : Very Busy Expressions Analysis, lv : Analyse de vivacité ́).

`./gradlew --console=plain --quiet run --args "src/main/While/exAE_1.txt ae"`

`./gradlew --console=plain --quiet run --args "src/main/While/exRD_1.txt rd"`

## Structure du projet

### Package `antlr`

Le package contient la grammaire du langage While.

###  Package `java`
 
- Le package `analyse` contient toutes les classes permettant d'obtenir des informations sur le cadre monotone (étiquettes initiale(s) et finale(s), flow, ...).


- Le package `ast` contient toutes les classes représentant la syntaxe du langage WHILE.


- Le package `framework` contient toutes les classes représentant le cadre monotone avec :
  - `AEVisitor` pour l'analyse de disponibilité des expressions.
  - `LVVisitor` pour l'analyse de vivacité.
  - `RDVisitor` pour l'analyse de la validité des expressions.
  - `VBVisitor` pour Very Busy expression analysis.
  - `DDVisitor` est utilisée pour l'analyse des procédures pour la disponibilité des expressions et very busy expressions.
  - `MFP` applique l'algorithme MFP.


- Le package `printer` contient une classe permettant d'afficher sur console le programme donné en paramètre avec les étiquettes sur chaque instruction.


- Le package `support` contient des classes utilitaires.


- Le package `While` contient la classe `Main`.


### Package `While`

Le package contient tous les exemples de programmes à utiliser avec l'analyseur.

- `ex1.txt` jusqu'à `ex3.txt` représentent les exemples du sujet.
- Les fichiers `exAE*.txt` sont spécifiques à l'analyse de disponibilité des expressions.
- Les fichiers `exLV*.txt` sont spécifiques à l'analyse de vivacité.
- Les fichiers `exRD*.txt` sont spécifiques à l'analyse de la validité des expressions.
- Les fichiers `exVB*.txt` sont spécifiques à Very Busy expression analysis.

## Question 1

Nous utilisons ANTLR pour faire l'analyse lexicale et syntaxique du langage WHILE.
L'arbre abstrait contient les types suivants contenus dans le package ast:

- AExpBinary : Expression arithmétique binaire
- AExpConst : Expression arithmétique avec contantes
- AExpIdent : Expression arithmétique avec variables
- AExpOpposite : Expression arithmétique négatif
- AExpParenthesis : Expression arithmétique avec parenthèses
- Affectation : Affectation d'une valeur à une variable
- Begin : begin
- BExpBinary : Expression binaire
- BExpFalse : Expression binaire false
- BExpNot : : Expression binaire not
- BExpParenthesis : Expression binaire avec parenthèses
- BExpTrue : Expression binaire true
- BlockOnly : block seulement
- BlockParenthesis : block avec parenthèses
- Boolean : type boolean
- Build : builder
- Call : appel Call
- Constant : constante
- Declaration : déclaration global
- DeclVariables : déclaration de variables
- End : end
- Identifier : variable
- If : condition if
- Int : type entier
- LAexpression : liste expression arithmétique
- LDeclIdent : liste expression arithmétique avec variable
- LDeclVariables  : liste déclarations variables
- LIdentifier : liste de variables
- Program : programme
- Skip : skip
- Statement : instructions
- Statements : liste d'instructions
- While : condition while


## Remarques sur les appels de procédure et retours

Pour la disponibilité et la validité des expressions, l'analyse des procédures est simplifiée en combinant le contexte d'appel et le retour.
Quant aux deux autres analyses, le contexte d'appel est le seul pris en compte.